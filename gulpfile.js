// https://github.com/idmytro/tailwindcss-gulp-starter
// https://gist.github.com/taylorbryant/91fc05b12472a88a8b6494f610647cd4

import gulp from 'gulp';
import sourcemaps from 'gulp-sourcemaps';
import clean from 'gulp-clean';
import nunjucksRender from 'gulp-nunjucks-render';
import browserSyncPackage from 'browser-sync';
import postcss from 'gulp-postcss';
import atimport from 'postcss-import';
import stripCssComments from 'gulp-strip-css-comments';
import footer from 'gulp-footer';
import tailwindcss from 'tailwindcss';
import tailwindcssNesting from 'tailwindcss/nesting/index.js';
import autoprefixer from 'autoprefixer';
import babel from 'gulp-babel';

import { SOURCE_NUNJUCKS_GLOBALS } from './src/js/globals/index.js';
import { SOURCE_NUNJUCKS_FILTERS } from './src/js/filters/index.js';

import { series } from 'gulp';

const browserSync = browserSyncPackage.create();

const TAILWIND_CONFIG = './config/tailwind.config.js';
const SOURCE_HTML_DIR = './docs/**.html';
const SOURCE_NUNJUCKS_PATHS = ['./src/templates/','./docs/'];
const SOURCE_NUNJUCKS_FILES = ['./src/templates/**/*','./docs/**/*'];
const SOURCE_NUNJUCKS_DIR = './docs/**/*.html';
const SOURCE_JS_FILES = ['./src/**/*.js','!./src/js/globals/**', '!./src/js/filters/**'];
const SOURCE_STYLESHEET = './src/css/styles.css';
const SOURCE_STYLESHEET_DIR = './src/**/*.css';
const DESTINATION_HTML_DIR = './dist/';
const DESTINATION_JS_DIR = './dist/';
const DESTINATION_STYLESHEET = './dist/css/';

const manageEnvironment = function(environment) {
  // Custom globals
  for (const key in SOURCE_NUNJUCKS_GLOBALS) {
    environment.addGlobal(key, SOURCE_NUNJUCKS_GLOBALS[key]);
  }

  // Custom filters
  for (const key in SOURCE_NUNJUCKS_FILTERS) {
    environment.addFilter(key, SOURCE_NUNJUCKS_FILTERS[key]);
  }
};

function bs(cb) {
  browserSync.init({
    server: {
      baseDir: './dist/',
    },
    notify: false
  });

  cb();
}

function watchFiles(cb) {
  gulp.watch([TAILWIND_CONFIG, SOURCE_STYLESHEET_DIR, SOURCE_HTML_DIR, ...SOURCE_JS_FILES, SOURCE_NUNJUCKS_DIR, ...SOURCE_NUNJUCKS_FILES], gulp.series(html, nunjucks, js, css, reload));
  cb();
}

function reload(cb) {
  browserSync.reload();
  cb();
}

function css() {
  return gulp.src(SOURCE_STYLESHEET)
    .pipe(
      postcss([
        atimport(),
        tailwindcssNesting(),
        tailwindcss(TAILWIND_CONFIG),
        autoprefixer()
      ])
    )
    .pipe(stripCssComments({preserve: false}))
    .pipe(footer('\n'))
    .pipe(gulp.dest(DESTINATION_STYLESHEET));
}

function html() {
  return gulp.src(SOURCE_HTML_DIR)
    .pipe(gulp.dest(DESTINATION_HTML_DIR));
}

function nunjucks() {
  return gulp.src(SOURCE_NUNJUCKS_DIR)
    .pipe(nunjucksRender({
        envOptions: {
          autoescape: true,
          trimBlocks: true,
          lstripBlocks: true,
          noCache: true
        },
        manageEnv: manageEnvironment,
        path: SOURCE_NUNJUCKS_PATHS // String or Array
      }))
    .pipe(gulp.dest(DESTINATION_HTML_DIR));
}

function js() {
  return gulp.src(SOURCE_JS_FILES, { sourcemaps: true })
    .pipe(babel())
    .pipe(gulp.dest(DESTINATION_JS_DIR));
}

function version() {
  return gulp.src('./package.json')
    .pipe(gulp.dest('./dist/'));
}

function license() {
  return gulp.src('./src/EUPL-1.2.txt')
    .pipe(gulp.dest('./dist/'));
}

function cleanFolder() {
  return gulp.src(DESTINATION_HTML_DIR, {read: false, allowEmpty: true})
    .pipe(clean());
}

export default series(cleanFolder, html, nunjucks, js, css, license, version, bs, watchFiles);
export const prod = series(cleanFolder, html, nunjucks, js, css, license, version, bs);

# README #

desy-html is the NPM library you need to start building a user interface for Gobierno de Aragón webapps. It uses Gulp, Postcss, Tailwindcss and Nunjucks to render html+css+js components. It's useful to create lightweight webapps or html mockups.

If you need a more powerful library based on this, use it's Angular port instead: [desy-angular](https://bitbucket.org/sdaragon/desy-angular)

See live examples of desy-html components: [https://desy.aragon.es/](https://desy.aragon.es/)

### How to set up? ###

* Run `npm install` first.
* Use `npm run dev` to generate CSS from `src/styles.css` to `/dist/styles.css`,  listen to changes in njk, css and html, and browser-sync reload.
* Use `npm run prod` to generate CSS, Purge it and Minify it.
* Dependencies: Node.js v20.14.0, Tailwind CSS and AutoPrefixer configed in PostCSS

### How do I start a project that uses desy-html components? ###

To start a new project that uses desy-html as dependency, don't use this repo, use **desy-html-starter** repo instead.

  * Download the desy-html-starter project from here: [https://bitbucket.org/sdaragon/desy-html-starter](https://bitbucket.org/sdaragon/desy-html-starter)
  * Personalize the downloaded project for your needings.
  * You'll be able to use any desy-html component in that project. Copy any macro example code from here and paste it inside any page, include or component in your project.

### Contact the team ###

* desy-html is maintained by a team at SDA Servicios Digitales de Aragón (Spain). If you want to know more about desy-html, please email any of the commiters.

### Software license ###
Unless stated otherwise, the codebase is released under the [EUPL-1.2 License](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12). This covers both the codebase and any sample code in the documentation.

### Thanks ###

This project started initially forked from: [https://github.com/saadeghi/tailwindcss-postcss-browsersync-boilerplate](https://github.com/saadeghi/tailwindcss-postcss-browsersync-boilerplate)

This project is built upon the GOV.UK Frontend project: [https://github.com/alphagov/govuk-frontend](https://github.com/alphagov/govuk-frontend) 
We would like to thank the Government Digital Service team for their fabulous work and for making it opensource for the community.

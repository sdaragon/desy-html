import { fontFamily as _fontFamily } from 'tailwindcss/defaultTheme'


// Helper funcions from: https://github.com/tailwindlabs/tailwindcss-typography/blob/master/src/styles.js
const round = (num) =>
  num
  .toFixed(7)
  .replace(/(\.[0-9]+?)0+$/, '$1')
  .replace(/\.0$/, '')
const rem = (px) => `${round(px / 16)}rem`
const em = (px, base) => `${round(px / base)}em`

export const content = ['./src/**/*.html',
  './src/**/*.njk',
  './src/**/*.js',
  './docs/**/*.html',
  './docs/**/*.njk',]
export const safelist = [
  'dev', // used in body tag
]
export const theme = {
  screens: {
    sm: '640px',
    md: '768px',
    lg: '1024px',
    xl: '1280px',
    // 2xl: '1536px',
    // 3xl: '1900px',
  },
  extend: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: '#ffffff',
      black: '#1f2331',
      neutral: {
        'dark': '#5e616b',
        'base': '#92949B',
        'light': '#ededec',
        'lighter': '#f6f6f5',
      },
      primary: {
        'base': '#00607a',
        'light': '#d6eaf0',
        'dark': '#00475C',
      },
      success: {
        'base': '#24d14c',
        'light': '#dcf8e2',
        'dark': '#1aa23a',
      },
      warning: {
        'base': '#fdcb33',
        'light': '#fef6b2',
        'dark': '#b88e12',
      },
      info: {
        'base': '#fa9902',
        'light': '#feebcc',
        'dark': '#c97a00',
      },
      alert: {
        'base': '#d22333',
        'light': '#fbd3ce',
        'dark': '#a40014',
      },
      heading: {
        'base': '#3c4c5c',
        'dark': '#26374a'
      }
    },
    spacing: {
      // the order of this declarations matter in specificity
      'base': '1rem', // 16px  // p-4
      'sm': '.5rem', //  8px  // p-2
      'xs': '.25rem', //  4px  // p-1
      'lg': '1.75rem', // 28px  // p-7
      'xl': '2.5rem', // 40px  // p-10
      '2xl': '5rem', // 80px  // p-20
      '3xl': '10rem', // 160px  // p-40
      'offcanvas': '83.333333%',
      'offcanvas-negative': '16.666667%',
    },
    borderRadius: {
      DEFAULT: '0.1875rem',
    },
    borderWidth: {
      '0.125em': '0.125em',
    },
    borderColor: theme => ({
      DEFAULT: (theme('colors.black'), theme('colors.currentColor'))
    }),
    boxShadow: theme => ({
      'outline-warning': '0 0 0 3px' + theme('colors.warning.base'),
      'outline-alert': '0 0 0 2px' + theme('colors.alert.base'),
      'solid-primary-base': '0 1px 0 0' + theme('colors.primary.base'),
      'solid-primary-light': '0 1px 0 0' + theme('colors.primary.light'),
      'solid-primary-dark': '0 1px 0 0' + theme('colors.primary.dark'),
      'solid-alert-base': '0 1px 0 0' + theme('colors.alert.base'),
      'solid-alert-dark': '0 1px 0 0' + theme('colors.alert.dark'),
      'solid-neutral-base': '0 1px 0 0' + theme('colors.neutral.base'),
      'solid-black': '0 1px 0 0' + theme('colors.black'),
      'outline-black': '0 0 0 3px' + theme('colors.black'),
      'outline-focus': 'inset 0 -2px 0 0 ' + theme('colors.black'),
      'outline-focus-input': 'inset 0 0 0 3px' + theme('colors.black')
    }),
    fontFamily: {
      sans: ['Open Sans', ..._fontFamily.sans],
    },
    gridTemplateColumns: {
      'max-content-1': 'repeat(1, minmax(0, max-content))',
      'max-content-2': 'repeat(2, minmax(0, max-content))',
      'max-content-3': 'repeat(3, minmax(0, max-content))',
      'max-content-4': 'repeat(4, minmax(0, max-content))',
      'max-content-5': 'repeat(5, minmax(0, max-content))',
      'max-content-6': 'repeat(6, minmax(0, max-content))',
      'max-content-7': 'repeat(7, minmax(0, max-content))',
      'max-content-8': 'repeat(8, minmax(0, max-content))',
    },
    gridTemplateRows: {
      '7': 'repeat(7, minmax(0, 1fr))',
      '8': 'repeat(8, minmax(0, 1fr))',
      '9': 'repeat(9, minmax(0, 1fr))',
      '10': 'repeat(10, minmax(0, 1fr))',
    },
    width: {
      'xs': '20rem',
      'sm': '24rem',
      'md': '28rem',
      'lg': '32rem',
      'xl': '36rem',
      'offcanvas': '83.333333%',
    },
    maxWidth: {
      '40': '10rem',
      '64': '16rem',
    },
    maxHeight: {
      '40': '10rem',
      '64': '16rem',
    },
    minHeight: {
      '14': '3.5rem',
    },
    typography: (theme) => ({
      DEFAULT: {
        css: {
          color: theme('colors.black'),
          lineHeight: '1.5rem',
          '--tw-prose-bullets': theme('colors.black'),
          '--tw-prose-hr': theme('colors.neutral.base'),
          '--tw-prose-quote-borders': theme('colors.neutral.base'),
          '--tw-prose-th-borders': theme('colors.neutral.base'),
          '--tw-prose-td-borders': theme('colors.neutral.base'),
          a: {
            color: theme('colors.primary.base'),
            '&:hover': {
              color: theme('colors.primary.dark'),
            },
            '&:focus': {
              color: theme('colors.black'),
              backgroundColor: theme('colors.warning.base'),
              boxShadow: theme('boxShadow.outline-focus'),
              outline: 'none',
              textDecoration: 'none'
            },
          },
          'h1': {
            // From: https://github.com/tailwindlabs/tailwindcss-typography/blob/master/src/styles.js
            fontSize: em(30, 16),
            marginTop: '0',
            marginBottom: em(28, 30),
            lineHeight: round(38 / 30),
            fontWeight: '700',
          },
          'h2': {
            // From: https://github.com/tailwindlabs/tailwindcss-typography/blob/master/src/styles.js
            fontSize: em(24, 16),
            marginTop: em(32, 24),
            marginBottom: em(16, 24),
            lineHeight: round(30 / 24),
          },
          'h3': {
            // From: https://github.com/tailwindlabs/tailwindcss-typography/blob/master/src/styles.js
            fontSize: em(18, 16),
            marginTop: em(24, 18),
            marginBottom: em(8, 18),
            lineHeight: round(22 / 18),
            fontWeight: '700',
          },
          'h4': {
            // From: https://github.com/tailwindlabs/tailwindcss-typography/blob/master/src/styles.js
            marginTop: em(24, 16),
            marginBottom: em(8, 16),
            lineHeight: round(20 / 16),
            fontWeight: '700',
          },
          'ul > li::before': {
            backgroundColor: theme('colors.black'),
          },
          'ol > li::before': {
            color: theme('colors.black'),
          },
        },
      },
    }),
    backgroundImage: {
      'salud-lg': "url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iODAwIiBoZWlnaHQ9IjEyNSIgdmlld0JveD0iMCAwIDgwMCAxMjUiPjxkZWZzPjxjbGlwUGF0aCBpZD0iYiI+PHJlY3Qgd2lkdGg9IjgwMCIgaGVpZ2h0PSIxMjUiLz48L2NsaXBQYXRoPjwvZGVmcz48ZyBpZD0iYSIgY2xpcC1wYXRoPSJ1cmwoI2IpIj48cmVjdCB3aWR0aD0iODAwIiBoZWlnaHQ9IjEyNSIgZmlsbD0iIzA2ODU3ZSIvPjxwYXRoIGQ9Ik01MDguMiwxMDkuOTYxYzEuODYyLDEyLjM4NiwxOS44NTcsMTAuNTY1LDE3LjAwNS0zLjM4LTEuNjA5LTEzLjE2Ny0zLjY0NC0yNi4yODctNS4xMjQtMzkuNDY4LS43NDQtNi42MzYtMi4yNS0xMS4wMzYtMTAuNTctOS45NDktOC4wNTYsMS4wNDgtMTEuMDIzLDUuMTg3LTkuNjI2LDEyLjYsMi41MjgsMTMuNDQ3LDUuMzMyLDI2Ljg0NCw4LjMxNyw0MC4yIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyMjIuMjE1IC0yLjk3MikiIGZpbGw9IiMwYjZjNjQiLz48cGF0aCBkPSJNMTEyMjcuOSwxNDQuNDA2aDBhMTIuNzIsMTIuNzIsMCwwLDEtNS44LTEuNjJjLTMuMzYyLTEuNzQ4LTUuNDU2LTMuOTU3LTYuMjI1LTYuNTY4YTkuNjE5LDkuNjE5LDAsMCwxLS4wOTItNC43NSwxOS4wNzksMTkuMDc5LDAsMCwxLDIuMTc4LTUuMzc5YzIuODkxLTUuMDY1LDUuNzQtMTAuMjY4LDguNS0xNS4zbC4wMjktLjA1M2MyLjkxNC01LjMyMiw1LjkyOC0xMC44MjUsOS4wMjktMTYuMjM1aDIwLjAxM2MtMi4zMTksMTMuMzMtMTQuMTg3LDM1LjQtMTguMDg3LDQyLjY1MmExNi44NCwxNi44NCwwLDAsMS00LjIwNyw1LjM0OEE4LjUsOC41LDAsMCwxLDExMjI3LjksMTQ0LjQwNloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMDU0OSAtOTUpIiBmaWxsPSIjMGI2YzY0IiBzdHJva2U9InJnYmEoMCwwLDAsMCkiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgc3Ryb2tlLXdpZHRoPSIxIi8+PHBhdGggZD0iTTI4Ni4wNjMsODIuOWMzLjAxLTIuMDMxLDUuNjk1LTguMTg0LDIuNi0xMi41MzlhMTg4LjgzOCwxODguODM4LDAsMCwwLTE5LTI5LjI4M2MtMS42NzItMi4xMjQtNy40LTUuMDU3LTExLjkxOC0zLjExNi0xLjUyMy42NTQtNC42MzgsNC43NC00LjYzOCw0Ljc0LTEuNjcxLDIuNzc5LTEuNjEzLDYuMzA1LDEuNzA4LDEwLjIxNiwzLjUsNC4xMjEsMTQuMTQ5LDIwLjM0NywxOS4wNTMsMjYuMjQsNS45MjYsNy4xODEsMTIuMiwzLjc0MiwxMi4yLDMuNzQyIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg4OS40NDEgMC45MjIpIiBmaWxsPSIjMGI2YzY0Ii8+PHBhdGggZD0iTTMwNy41MjEsNDguMWMtMi45MTksMi40NTgtMi4xMDUsOC41NTgtMS45ODcsMTAuODIuMTA5LDIuMDU5LDIuODYxLDUuMDg4LDUuMDIxLDUuNzQ1QTI3NS43MzEsMjc1LjczMSwwLDAsMCwzMzkuNCw3MS44NTNjNC43NzMsMS4zNzYsNi45NDgtMS41NTMsNy42NzMtMy40OTUsMS4zMS0zLjUsMy40MjMtOC43NjMuMzkxLTExLjI4NEMzNDUuMiw1My44MTgsMzExLjcsNDQuNTg4LDMwNy41MjEsNDguMSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTMyLjkgMjYuMTQ3KSIgZmlsbD0iIzBiNmM2NCIvPjxwYXRoIGQ9Ik0xMTE3NC41NTUsMTk0LjVIMTExMDNjMy4yMjUtMjUuNTM3LDYuOTY0LTU1LjI4NiwxMC41MzUtODUuMi41MTktNC4zMzMsMS43MzctNy40MDUsMy43MjUtOS4zOTJzNC44MTMtMi45NTYsOC42My0yLjk1NmEyNy45MTUsMjcuOTE1LDAsMCwxLDIuOTM2LjE2NmMyNy4xNTgsMi44NzgsNTMuNyw1LjYyNyw3OS45LDcuNjIsMy44NzEuMjkxLDYuMzMxLjkzMSw4LjIyOCwyLjE0MSwyLjE2OSwxLjM4MiwzLjU5NCwzLjU2Myw0LjQ4NCw2Ljg2NGEyOTIuMTE5LDI5Mi4xMTksMCwwLDAsMjIuNiw0My45OTMsMjk4LjgzOSwyOTguODM5LDAsMCwwLDI3LjE1NSwzNi43NTdsLTgzLjkzMSwwYTU5LjczOCw1OS43MzgsMCwwLDEsMy45NDMtMTAuODY3LDc0LjA4NSw3NC4wODUsMCwwLDEsNS45NDYtMTAuMjI0LDEzLjEyNCwxMy4xMjQsMCwwLDEsMi45ODgtMy40MjgsOC40MTQsOC40MTQsMCwwLDAsLjg5MS0uNzcxLDguNzQyLDguNzQyLDAsMCwwLC4wNjctMTIuMzQ1bC0uMDA2LDBhNC41LDQuNSwwLDAsMC00LjAxLS42MTdjLTYuNzI3LDMuMjg5LTEwLjk0NCw3Ljc5MS0xMi45LDEzLjc2NS0uNSwxLjUzMi0xLjAzNiwzLjAzOS0xLjY1Nyw0Ljc4M2wtLjA0Mi4xMTctLjAyOS4wODFjLS4zODIsMS4wNjctLjc1LDIuMS0xLjA5NCwzLjA4MS0uOTQ4LTEuODA5LTEuODU5LTMuNTY2LTIuNzg3LTUuMzU3bC0uMTA3LS4yMDctLjA1Mi0uMWMtMi4xMTYtNC4wODMtNC4zLTguMzA1LTYuNTQyLTEyLjMxNWE0LjM2Myw0LjM2MywwLDAsMC0xLjUyNC0yLjE1MywzLjU2NCwzLjU2NCwwLDAsMC0yLjA5Mi0uNjIsOS4zMzQsOS4zMzQsMCwwLDAtNi4yODUsMy4yNzZjLS43NSwxLjEyMy42MjMsNC4wNTUsMS4yNDgsNS4yNjMuNzYsMS40NjIsMS42NjYsMi44NjIsMi41NDMsNC4yMTZsLjAxNS4wMjRhMzQuNDkxLDM0LjQ5MSwwLDAsMSwzLjQ4Myw2LjI4NSw4OSw4OSwwLDAsMSw1LjMsMTguMTA5Wm0yMTYuMDEyLDBoLTUuODE2YTExOS4xOCwxMTkuMTgsMCwwLDEtMTEuMjk1LTE5LjI2NCwxMDQuNDc0LDEwNC40NzQsMCwwLDEtNy43NzctMjMuNmMtMy44MDktMTkuMTcyLTEuNjk0LTM4LjU2LDYuMjg1LTU3LjYyOGE4Ny4xNzcsODcuMTc3LDAsMCwxLDE2LjA1NS0yNC4wODloMi41NDlaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTA1OTAuMDY2IC02OSkiIGZpbGw9IiMwYjZjNjQiIHN0cm9rZT0icmdiYSgwLDAsMCwwKSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2Utd2lkdGg9IjEiLz48cGF0aCBkPSJNMzA3LjgyNyw1MS4zNThjOC4yMTYtNi40MjEsMTYuMDQzLTEzLjM1MiwyNC4zNTUtMTkuNjQ2LDUuNzcxLTQuMzcyLDYuODc4LTguNzIyLDIuNTktMTQuODA4cy04LjkzNC02Ljc1MS0xNC44NDctMi42MzljLTQuNDUxLDMuMTA1LTkuMDY5LDUuOTYtMTUuMjU3LDEwbC0xMS42LDguMzg0Yy02LjcwNiw0Ljg1NS03LjQzNCwxMC40MzItMi40NzMsMTcuMTA5LDUuMjY5LDcuMDk0LDExLjE2Myw2LjM1NiwxNy4yMzQsMS42MDUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEyMi45NzEgNi40NDMpIiBmaWxsPSIjMGI2YzY0Ii8+PHBhdGggZD0iTTExNDIyLjY0OCwxMDMuMTEyaC0xNjguOTY0Yy0yLjc5My00LjU3My02LjAyLTkuOTEtOC45ODktMTUuNGExMTguMTkzLDExOC4xOTMsMCwwLDEtNy41MjItMTYuMzI1Yy0yLjA1OS01Ljc3MS0zLjAxLTEwLjc5NS0yLjgyNi0xNC45MzFhMTcuMzI2LDE3LjMyNiwwLDAsMSwxLjM1OC02LjIxNywxMy43NDUsMTMuNzQ1LDAsMCwxLDMuNzQ0LTUsMTkuMzUxLDE5LjM1MSwwLDAsMSw5LjgtMi41ODMsMjIuMjM2LDIyLjIzNiwwLDAsMSw4LjQ1NywxLjcwNSwyMS42NiwyMS42NiwwLDAsMSw3LjI4MSw0LjkyN2MyLjI1OSwyLjMyOCw0LjUsNC43MzMsNi42NjcsNy4wNTlhMjAzLjksMjAzLjksMCwwLDAsMTYuMjg0LDE2LjIwNmM0LjA2LDMuNDc4LDkuNzM0LDYuMzczLDE3LjM0Nyw4Ljg1YTExLjc0MSwxMS43NDEsMCwwLDAsMy42LjYzNSw1LjQsNS40LDAsMCwwLDQuNDg3LTEuOTU0YzEuMjQ5LTEuNjI2LDEuNS00LjE1OS43MDgtNy4xMzMtMS4zNTgtNS4xLTMuMDYtMTAuMi00LjctMTUuMTNsLS4wMTMtLjA0YTE5NS4zODgsMTk1LjM4OCwwLDAsMS01Ljg1OS0xOS45LDgwLjg3Nyw4MC44NzcsMCwwLDEtMS44OTEtMTIuNTg0LDMyLjcxNiwzMi43MTYsMCwwLDEsMS4yNDctMTIuMTU0LDE4LjQsMTguNCwwLDAsMSw2LjEyOS05QTE1LjEyMywxNS4xMjMsMCwwLDEsMTEzMTguMjY2LDFhMTguNTg1LDE4LjU4NSwwLDAsMSwxMS4xNzMsNC4wMTksMzUzLjAyMywzNTMuMDIzLDAsMCwxLDMyLDI0LjU1NiwzMDkuNzQyLDMwOS43NDIsMCwwLDEsMjUuNzcxLDI1LjAxLDI2NC45MjksMjY0LjkyOSwwLDAsMSwzNS40MzcsNDguNTI4djBoMFoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMTA2MSAyMi4zODgpIiBmaWxsPSIjMGI2YzY0IiBzdHJva2U9InJnYmEoMCwwLDAsMCkiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgc3Ryb2tlLXdpZHRoPSIxIi8+PHBhdGggZD0iTTE1Mi4yNDEsMGMtNy40MTgsOC4xMTEtNy44NzgsMjMuOC0xLjE5NCw1NC44MTIsMTIuMDI4LDU1LjgyNC03OC4zNDEsOS4zNzgtMTMxLjQ3Mi0zNy41QzEyLjUsMTEuMDYyLDUuOTkxLDUuMzE4LDAsMFoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDMuNTA3IDApIiBmaWxsPSIjMGI2YzY0Ii8+PC9nPjwvc3ZnPg==')",
      'salud': "url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iNTAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDUwMCAyMDAiPjxkZWZzPjxjbGlwUGF0aCBpZD0iYiI+PHJlY3Qgd2lkdGg9IjUwMCIgaGVpZ2h0PSIyMDAiLz48L2NsaXBQYXRoPjwvZGVmcz48ZyBpZD0iYSIgY2xpcC1wYXRoPSJ1cmwoI2IpIj48cmVjdCB3aWR0aD0iNTAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzA2ODU3ZSIvPjxwYXRoIGQ9Ik02MTQuMTEyLDE1LjQ3NiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTU1MS40OCA4LjU1MikiIGZpbGw9IiMwYjZjNjQiLz48cGF0aCBkPSJNMTA1NjAuMTY1LDE4NS4zODZoLTI2NC4zMjZjLTIuNTI1LTIuOTI1LTQuNzY5LTUuNjQ1LTYuODU4LTguMzE4YTY2LjMyMiw2Ni4zMjIsMCwwLDEtMTEuMjMtMjEuOSwyNS4yNCwyNS4yNCwwLDAsMS0xLTkuOTM1LDE2LjQ0NiwxNi40NDYsMCwwLDEsMi45ODgtNy45LDE2LjE0MiwxNi4xNDIsMCwwLDEsNi40MTUtNS4xNDgsMjQuMzgsMjQuMzgsMCwwLDEsOS41MzQtMi4wNTFjLjMzMy0uMDA5LjY3Mi0uMDE0LDEuMDA4LS4wMTQsNy40NjEsMCwxNS4xMTMsMi4zLDIyLjUxMyw0LjUzM2wuMTEuMDMzYzEuMTM4LjM0MiwyLjMxMy43LDMuNDYxLDEuMDMzYTExOC4wNzksMTE4LjA3OSwwLDAsMSwxMS4yNDUsNC4xNWwuMDA2LDBjNC45MDcsMiw5Ljk4LDQuMDcyLDE1LjE4LDUuMTIyYTI1LjcsMjUuNywwLDAsMCw0Ljk2Mi40NDUsMjcuNjI0LDI3LjYyNCwwLDAsMCw3LjE1Mi0uODgyYzIuMzI1LS42NDYsMy45NDgtMS42LDQuNjkzLTIuNzU1LDIuNzI0LTQuMjE5LDEuODg2LTEyLjc4NS0uNjA1LTE4LjEzOS0xLjUzNC0zLjI4Mi00LjQyMi04LTcuNzY2LTEzLjQ3bC0uMDA5LS4wMTRjLTYuMTI2LTEwLjAxOC0xMy43NTEtMjIuNDg2LTE4LjA4My0zNC4xMTRhNjEuODUyLDYxLjg1MiwwLDAsMS0yLjU0NS04LjQ5NSwzMy40NTUsMzMuNDU1LDAsMCwxLS44MjItNy44MTEsMTguNjQzLDE4LjY0MywwLDAsMSwxLjM2NC02Ljc5MywxNC40NTEsMTQuNDUxLDAsMCwxLDQuMDE3LTUuNDQxLDIwLjM1NCwyMC4zNTQsMCwwLDEsMTAuMy0yLjcxNSwyMy4zODMsMjMuMzgzLDAsMCwxLDguODkzLDEuNzkzLDIyLjc2NywyMi43NjcsMCwwLDEsNy42NTYsNS4xOGMyLjM3NywyLjQ1MSw0LjczMyw0Ljk4LDcuMDEzLDcuNDI1bC4wMTEuMDEyYTIxNS4wMzIsMjE1LjAzMiwwLDAsMCwxNy4xMTEsMTcuMDNjNC4yNjEsMy42NSwxMC4yMjcsNi42OTQsMTguMjQsOS4zYTEyLjM0MSwxMi4zNDEsMCwwLDAsMy43OTEuNjY4LDUuNjcxLDUuNjcxLDAsMCwwLDQuNzE2LTIuMDU0YzEuMzE0LTEuNzA5LDEuNTc5LTQuMzc0Ljc0Ni03LjUtMS40My01LjM2OS0zLjIxOS0xMC43MzMtNC45NDktMTUuOTIxbC0uMDA3LS4wMjFhMjA1LjY0NiwyMDUuNjQ2LDAsMCwxLTYuMTY1LTIwLjkzNiw4NS4yMTIsODUuMjEyLDAsMCwxLTEuOTg3LTEzLjIzNCwzNC4zNzYsMzQuMzc2LDAsMCwxLDEuMzExLTEyLjc3OCwxOS4zNTIsMTkuMzUyLDAsMCwxLDYuNDQ1LTkuNDY4QTE1LjksMTUuOSwwLDAsMSwxMDQyNC40NDMsMWExOS41NDIsMTkuNTQyLDAsMCwxLDExLjc1LDQuMjI4LDM3MC41MjgsMzcwLjUyOCwwLDAsMSwzNC40MjYsMjYuNDkzLDMyNC4xNTUsMzI0LjE1NSwwLDAsMSwyNy41NTMsMjYuOTc0LDI3Ni41NjYsMjc2LjU2NiwwLDAsMSwzNy40MTUsNTIuMTU4LDIyNS4wMzcsMjI1LjAzNywwLDAsMSwxOC43MTIsNDQuMzg1YzQuMzMsMTQuNjc5LDUuNTM0LDI1LjE4OCw1Ljg2NiwzMC4xNDdaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTAwODcgMTQuNTk0KSIgZmlsbD0iIzBiNmM2NCIgc3Ryb2tlPSJyZ2JhKDAsMCwwLDApIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS13aWR0aD0iMSIvPjxwYXRoIGQ9Ik0xMDQzNC45OTQsMTYyLjQ3MmMtNi44MDgsMC0xNS40Mi0xLjkxMS0yNS42LTUuNjhhMjI2LjUxLDIyNi41MSwwLDAsMS0zMS41OC0xNC45OSwzNzQuNjM4LDM3NC42MzgsMCwwLDEtMzMuMzQ2LTIxLjIyMywzNzUuNzU1LDM3NS43NTUsMCwwLDEtMzAuODg4LTI0LjM3OGMtMy45LTMuNDM4LTcuMzg3LTYuNTItMTAuOTg2LTkuN2gxNDUuODczYy0yLjksOS43MzQtMS44NTgsMjQuODg5LDMuMzY4LDQ5LjE0YTQwLjA1Myw0MC4wNTMsMCwwLDEsLjk2NiwxMi44MzQsMTYuMjI0LDE2LjIyNCwwLDAsMS0zLjI1Nyw4LjQ2NUMxMDQ0Ni41ODIsMTYwLjYxLDEwNDQxLjY4NiwxNjIuNDcyLDEwNDM0Ljk5NCwxNjIuNDcyWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTEwMjU1IC04NykiIGZpbGw9IiMwYjZjNjQiIHN0cm9rZT0icmdiYSgwLDAsMCwwKSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2Utd2lkdGg9IjEiLz48L2c+PC9zdmc+')",
      'general-lg': "url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iODAwIiBoZWlnaHQ9IjEyNSIgdmlld0JveD0iMCAwIDgwMCAxMjUiPjxkZWZzPjxjbGlwUGF0aCBpZD0iYiI+PHJlY3Qgd2lkdGg9IjgwMCIgaGVpZ2h0PSIxMjUiLz48L2NsaXBQYXRoPjwvZGVmcz48ZyBpZD0iYSIgY2xpcC1wYXRoPSJ1cmwoI2IpIj48cmVjdCB3aWR0aD0iODAwIiBoZWlnaHQ9IjEyNSIgZmlsbD0iIzNjNGM1YyIvPjxwYXRoIGQ9Ik0xMTMyOS4yMjctMTIxOS4xNzgsMTExOTUuMy0xMjk2LjVoMTMzLjkzbDAsNTQuNHYyMi45MjRoMFoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMTE5MyAxMjk2KSIgZmlsbD0iIzI2Mzc0YSIgc3Ryb2tlPSJyZ2JhKDAsMCwwLDApIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS13aWR0aD0iMSIvPjxwYXRoIGQ9Ik0xMDcuNzcxLDg2LjI1OWg2OS40MTRjLTExLjYzNS0yMC4xMzUtMjMuMDc0LTM5LjkzMS0zNC43MjMtNjAuMDktMTEuNjc4LDIwLjIzLTIzLjA5Myw0MC0zNC42OTEsNjAuMDkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDM1NC4yMDcgLTEuMTA2KSIgZmlsbD0iIzI2Mzc0YSIvPjxwYXRoIGQ9Ik04My4zMzIsNzIuODY0LDcxLjQyNiw5My40N0g5NC40OTRjLTMuMzYxLTUuOTMzLTYuOC0xMy4yOTItMTAuMjIyLTE5LjE5Mi0uMjI4LS4zOTMtLjUtLjc2LS45NC0xLjQxNSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMzM0LjUwOCAtNTguMTA0KSIgZmlsbD0iIzI2Mzc0YSIvPjxwYXRoIGQ9Ik0xMTI5NS4xNzctMTM2My45OWgtNjcuODE0YTE2MC40MjksMTYwLjQyOSwwLDAsMSwxOC44MTEtMzMuMzI3bDE2LjExNS0yMS45MzFhNjguNDYzLDY4LjQ2MywwLDAsMSwxNi4yODctMTUuODMsNjcuNzExLDY3LjcxMSwwLDAsMSwyMC41LTkuNTgyYzcuNC0yLjA0NCwxMy40MzItMy43MTYsMTguOTc2LTUuMjYyYTE5Mi44NywxOTIuODcsMCwwLDEsNTEuNzY0LTcuMDc4LDE5My4xMTQsMTkzLjExNCwwLDAsMSw0NC40NzQsNS4ybDkuMTMxLDIuMTY0YTExMy45LDExMy45LDAsMCwxLDI1LjMyOCw5LjI5MiwxMTQuNjQ3LDExNC42NDcsMCwwLDEsMjIuMjA4LDE0Ljc4NUExMTUuNTc5LDExNS41NzksMCwwLDEsMTE0ODkuMi0xNDA2YTExNi4zOTQsMTE2LjM5NCwwLDAsMSwxMy40MzMsMjMuNjE5LDE0NS4wNzksMTQ1LjA3OSwwLDAsMSw2LjM1MiwxOC4zOTFoLTc1LjI2MWE2NC4zLDY0LjMsMCwwLDAtNi41NDgtMTAuNDg4LDYzLjkxNiw2My45MTYsMCwwLDAtOC4zNS04Ljk1Miw2My41MTQsNjMuNTE0LDAsMCwwLTkuOS03LjE4LDYzLjE3NCw2My4xNzQsMCwwLDAtMTEuMTkzLTUuMTczbC04LjcwNy0zLjAzMWE2Mi42MTYsNjIuNjE2LDAsMCwwLTIwLjYzNS0zLjQ5MSw2Mi44MzgsNjIuODM4LDAsMCwwLTE1LjQ2NSwxLjkzNSw2My4wMzcsNjMuMDM3LDAsMCwwLTE0LjY2OSw1Ljc2NWwtMjIsMTIuMDExYTYzLjQ4Myw2My40ODMsMCwwLDAtMTEuNjM5LDguMTcyLDY0LDY0LDAsMCwwLTkuNDQ2LDEwLjQzMloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMTA3NiAxNDg5LjQ4OSkiIGZpbGw9IiMyNjM3NGEiIHN0cm9rZT0icmdiYSgwLDAsMCwwKSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2Utd2lkdGg9IjEiLz48cGF0aCBkPSJNMTEzNTAuMDg0LTEzMTIuNWgtMjI0LjU0M2MtMTMuNDg0LTE4LjY5LTIwLjg4MS0yOS43LTIxLjk4Ni0zMi43MTZhOC4yNiw4LjI2LDAsMCwxLC41OTUtNy4xODljMS4yODQtMi4yNzMsMy42NjYtNC40NjQsNy4wNzctNi41MTFhNTcuNTc4LDU3LjU3OCwwLDAsMSwxMC4wNzEtNC42LDk5Ljc2Miw5OS43NjIsMCwwLDEsMTIuOTE5LTMuNjJsNjUuMjgxLDQ0LjlMMTExODIuMi0xNDM4LjVoNDIuMTczYzYuMjU4LDYuNDkzLDEzLjMzLDEzLjg5MSwyMS4wMjEsMjEuOTksMjEuMywyMi40MjMsNDEuMjM4LDQzLjc2LDQ1LjAxOSw0Ny44MWwuMTU5LjE3MWEuMDYxLjA2MSwwLDAsMS0uMDA3LS4wMTVsMC0uMDA5YS4yODkuMjg5LDAsMCwwLS4wMTMtLjAzMWwtLjAyNS0uMDYzYy0uMDA3LS4wMTYtLjAxNC0uMDMzLS4wMjEtLjA1MnMtLjAyNy0uMDY5LS4wNDUtLjExMWwtLjA0Ni0uMTE0Yy0yLjU5NC02LjQ0OC0xMS43NTctMjkuMjg1LTIwLjc2NC01Mi40MjctMi4zMjQtNS45NzctNC41NDgtMTEuNzQ3LTYuNjExLTE3LjE0OWgzOS4yM2w0Ny44MTQsODUuNzEzdjQwLjI4N1oiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMDU0OS41ODIgMTQzOCkiIGZpbGw9IiMyNjM3NGEiIHN0cm9rZT0icmdiYSgwLDAsMCwwKSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2Utd2lkdGg9IjEiLz48L2c+PC9zdmc+')",
      'general': "url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iNTAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDUwMCAyMDAiPjxkZWZzPjxjbGlwUGF0aCBpZD0iYiI+PHJlY3Qgd2lkdGg9IjUwMCIgaGVpZ2h0PSIyMDAiLz48L2NsaXBQYXRoPjwvZGVmcz48ZyBpZD0iYSIgY2xpcC1wYXRoPSJ1cmwoI2IpIj48cmVjdCB3aWR0aD0iNTAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzNjNGM1YyIvPjxwYXRoIGQ9Ik0xMDM5My41LTE3NzAuNGgtMTM5LjM2OHExMy40NTMtMjMuMzA1LDI2LjgwNy00Ni40M2w1LjYyOC05Ljc0OCwxMS44MzgtMjAuNSwyLjM0Ny00LjA2NywyNy45MTYtNDguMzU0aDEzLjk3OXE2Ljk3NywxMi4wNzUsMTMuOTI1LDI0LjA5NWw1LjAyLDguNjg5cTQuNDE4LDcuNjQ4LDguODMyLDE1LjI4NmwyLjkxOSw1LjA1MS41MDguODc5cTUuMzY0LDkuMjgxLDEwLjcyNiwxOC41NjJsOC45MjUsMTUuNDQzdjQxLjFaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOTg5MyAxODk5KSIgZmlsbD0iIzI2Mzc0YSIgc3Ryb2tlPSJyZ2JhKDAsMCwwLDApIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS13aWR0aD0iMSIvPjxwYXRoIGQ9Ik05NS44MTQsNzIuODY0LDcxLjQyNiwxMTUuMDcxaDQ3LjI1Yy02Ljg4NC0xMi4xNTItMTMuOTMzLTI3LjIyNS0yMC45MzctMzkuMzEtLjQ2Ny0uOC0xLjAzLTEuNTU2LTEuOTI1LTIuOSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjE2LjYzNSAtNTcuOTY4KSIgZmlsbD0iIzI2Mzc0YSIvPjxwYXRoIGQ9Ik0xMDg0MS4yOTItMTcyNS41aC0xMTguMzgzYTEwNC44NTcsMTA0Ljg1NywwLDAsMC03Ljg0OS0yOC4wNDMsMTA0LjYzMSwxMDQuNjMxLDAsMCwwLTEwLjUzNS0xOC42MTYsMTAzLjY0MSwxMDMuNjQxLDAsMCwwLTEzLjkzMS0xNS44ODQsMTAzLjE2MywxMDMuMTYzLDAsMCwwLTE2LjgzLTEyLjY5LDEwMi40NDEsMTAyLjQ0MSwwLDAsMC0xOS4yMzctOS4wMzZsLTE0LjExMi00LjkxNGExMDEuNTE1LDEwMS41MTUsMCwwLDAtMzMuNDQ3LTUuNjU5LDEwMS44MTMsMTAxLjgxMywwLDAsMC0yNS4wNjYsMy4xMzcsMTAyLjEwOSwxMDIuMTA5LDAsMCwwLTIzLjc4LDkuMzQ2bC0zLjYyMSwxLjk3N3YtOTguMjc4YTMxMy4zMDksMzEzLjMwOSwwLDAsMSw1NC43NzUtNC44MzksMzEyLjk3MywzMTIuOTczLDAsMCwxLDcyLjA5Myw4LjQyOWwxNC44LDMuNWExODQuNjU1LDE4NC42NTUsMCwwLDEsNDEuMDU3LDE1LjA2MiwxODUuNzU3LDE4NS43NTcsMCwwLDEsMzYsMjMuOTY1LDE4Ny4yNDUsMTg3LjI0NSwwLDAsMSwyOS41NjksMzEuNzA2LDE4OC41MiwxODguNTIsMCwwLDEsMjEuNzcyLDM4LjI4NywyMzQuMjgzLDIzNC4yODMsMCwwLDEsMTYuNzI4LDYyLjU0NloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMDU1NSAxOTI2KSIgZmlsbD0iIzI2Mzc0YSIgc3Ryb2tlPSJyZ2JhKDAsMCwwLDApIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS13aWR0aD0iMSIvPjxwYXRoIGQ9Ik02MTQuMTEyLDE1LjQ3NiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTU1MS40OCA1LjU1MikiIGZpbGw9IiMyNjM3NGEiLz48L2c+PC9zdmc+')",
    },
  },
}
export const plugins = [
  require('@tailwindcss/forms'),
  require('@tailwindcss/typography'),
]

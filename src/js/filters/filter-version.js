
import { readFileSync } from 'fs';
import { join } from 'path';

export default function() {
  const pkg = JSON.parse(readFileSync(join(process.cwd(), 'package.json'), 'utf8'));
  return pkg.version;
}

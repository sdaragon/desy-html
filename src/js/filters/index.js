/**
 * Nunjucks filters
 */

import highlight from './highlight.js';
import filtercaller from './filter-caller.js';
import filterslugify from './filter-slugify.js';
import filterquotes from './filter-quotes.js';
import filterescapeltgt from './filter-escape-ltgt.js'
import filterversion from './filter-version.js'

export const SOURCE_NUNJUCKS_FILTERS = {
  highlight,
  filtercaller,
  filterslugify,
  filterquotes,
  filterescapeltgt,
  filterversion
};

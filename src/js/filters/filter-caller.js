function filtercaller (params) {
  const newParams = {};
  for(var k in params) newParams[k]=params[k];
  delete newParams.caller;
  return newParams;
}

export default filtercaller;

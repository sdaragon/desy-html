function filterquotes(str) {
  if (typeof str !== 'string') return str;

  // State to track if we are inside an HTML tag
  let inTag = false;
  let result = '';
  let currentQuote = null;

  for (let i = 0; i < str.length; i++) {
    const char = str[i];

    if (char === '<') {
      inTag = true;
      result += char;
      continue;
    }

    if (char === '>') {
      inTag = false;
      result += char;
      continue;
    }

    if (inTag) {
      // If we find a quote inside a tag
      if (char === "'" || char === '"') {
        if (currentQuote === null) {
          // Starting an attribute value
          currentQuote = char;
          result += '"'; // Always use double quotes
        } else if (currentQuote === char) {
          // Closing the attribute value
          currentQuote = null;
          result += '"';
        } else {
          // It's a quote inside the attribute value
          result += char;
        }
      } else {
        result += char;
      }
    } else {
      result += char;
    }
  }

  return result;
}

export default filterquotes;
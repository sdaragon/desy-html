function filterslugify(str) {
  // Replace spaces with hyphens
  let slug = str.replace(/\s+/g, '-');
  // Remove special characters
  slug = slug.replace(/[^\w-]+/g, '');
  // Convert to lowercase
  slug = slug.toLowerCase();
  return slug;
}

export default filterslugify;

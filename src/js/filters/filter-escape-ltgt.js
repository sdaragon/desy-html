function filterescapeltgt(str) {
  if (typeof str !== 'string' && typeof str !== 'object') return str;
  if (typeof str !== 'string') str = JSON.stringify(str);
  return str.replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

export default filterescapeltgt;
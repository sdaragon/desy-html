// Dependencies for dropdownComponent and listboxComponent:
// https://unpkg.com/@popperjs/core@2.4.4/dist/umd/popper.min.js
// https://unpkg.com/tippy.js@6.2.6/dist/tippy-bundle.umd.min.js


import {
  accordionComponent,
  alertComponent,
  collapsibleComponent,
  dialogComponent,
  dropdownComponent,
  listboxComponent,
  linksListComponent,
  menubarComponent,
  tableAdvancedComponent,
  tabsComponent,
  tooltipComponent,
  toggleComponent,
  treeComponent,
  notificationComponent,
  radioButtonComponent,
  checkBoxComponent,
  MenuVerticalComponent,
  MenuHorizontalComponent,
  MenuNavigationComponent,
  HeaderNavigationComponent,
  NavComponent,
  DatepickerComponent
} from './desy-html.js';

var aria = aria || {};

accordionComponent(aria);
alertComponent(aria);
collapsibleComponent(aria);
dialogComponent(aria);
dropdownComponent(aria);
listboxComponent(aria);
linksListComponent(aria);
menubarComponent(aria);
tableAdvancedComponent(aria);
tabsComponent(aria);
tooltipComponent(aria);
toggleComponent(aria);
treeComponent(aria);
notificationComponent(aria);
radioButtonComponent(aria);
checkBoxComponent(aria);
MenuVerticalComponent(aria);
MenuHorizontalComponent(aria);
MenuNavigationComponent(aria);
HeaderNavigationComponent(aria);
NavComponent(aria);
DatepickerComponent(aria);

//Using the browser API to copy code to the clipboard
const copyButtons = document.querySelectorAll('[data-module = "c-button-copy"]');
copyButtons.forEach(button => {
  button.addEventListener("click", async (event) => {
    await copyCode(button);
  });
});

async function copyCode(button) {
  const code = button.nextElementSibling.innerText;
  await navigator.clipboard.writeText(code);
  setTimeout(() => {
    button.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" class="self-center mr-2" aria-hidden="true" width="1em" height="1em"><path d="M13.714 42.857A6.857 6.857 0 0 1 8.4 40.183L.857 31.646a3.429 3.429 0 0 1 .309-4.869A3.429 3.429 0 0 1 6 27.12l7.063 7.989a.72.72 0 0 0 .617.308.789.789 0 0 0 .617-.274L42.103 6.206a3.429 3.429 0 0 1 4.937 4.731L18.926 40.526a6.651 6.651 0 0 1-5.212 2.331Z" fill="currentColor"></path></svg> Copiado';
  },100)

  setTimeout(() => {
    button.innerHTML = "Copiar código";
  },2500)

}

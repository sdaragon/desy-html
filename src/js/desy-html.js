import { utils, formatDateToDDMMYYYY, convertToISOFormat } from './aria/utils.js';
import { accordion } from './aria/accordion.js';
import { dataGrid } from './aria/dataGrid.js';
import { dialog } from './aria/dialog.js';
import { tabs } from './aria/tabs.js';
import { PopupMenuItemAction } from './aria/PopupMenuItemAction.js';
import { PopupMenuAction } from './aria/PopupMenuAction.js';
import { MenubarItemAction } from './aria/MenubarItemAction.js';
import { MenubarAction } from './aria/MenubarAction.js';
import { listbox } from './aria/listbox.js';
import { LinksList } from './aria/linksList.js';
import { alert } from './aria/alert.js';
import { Treeitem } from './aria/treeitem.js';
import { Tree } from './aria/tree.js';
import { Toggle } from './aria/toggle.js';
import { Collapsible } from './aria/collapsible.js';
import { Notification } from './aria/notification.js';
import { RadioButton } from './aria/radioButton.js';
import { CheckBox } from './aria/checkBoxes.js';
import { MenuVertical } from './aria/MenuVertical.js';
import { HeaderNavigation } from './aria/HeaderNavigation.js';
import { MenuHorizontal } from './aria/MenuHorizontal.js';
import { MenuNavigation } from './aria/MenuNavigation.js';
import { Nav } from './aria/Nav.js';

export function accordionComponent(aria) {
  accordion(aria);
}

export function alertComponent(aria) {
  alert(aria);
}

export function toggleComponent(aria) {
  Toggle(aria);
}

export function collapsibleComponent(aria) {
  Collapsible(aria);

  const collapsibles = document.querySelectorAll('[data-module="c-collapsible"]');
  [...collapsibles].forEach((collapsible) => {
    aria.collapsibleInit(collapsible);
  });
}

export function dialogComponent(aria) {
  utils(aria);
  dialog(aria);
}

export function dropdownComponent(aria) {
  const modules = document.querySelectorAll('[data-module]');
  for (const item in modules) if (modules.hasOwnProperty(item)) {
    const moduleValue = modules[item].getAttribute('data-module');


    if (moduleValue == 'c-dropdown'){
      const buttonDropdown = modules[item].querySelector('[data-module = "c-dropdown-button"]');
      const tooltip = modules[item].querySelector('[data-module = "c-dropdown-tooltip"]');
      const roleCustom = buttonDropdown.getAttribute('data-role');
      const ariaLabel = buttonDropdown.getAttribute('data-aria-label');
      const ariaModal = buttonDropdown.getAttribute('data-aria-modal');
      const ariaHasPopup = buttonDropdown.getAttribute('data-aria-haspopup');
      if(buttonDropdown && tooltip) {
        const hideOnPopperBlur = {
          /* https://atomiks.github.io/tippyjs/v6/plugins/#hideonpopperblur */
          name: 'hideOnPopperBlur',
          defaultValue: true,
          fn(instance) {
            return {
              onCreate() {
                instance.popper.addEventListener('focusout', (event) => {
                  if (
                    instance.props.hideOnPopperBlur &&
                    event.relatedTarget &&
                    !instance.popper.contains(event.relatedTarget)
                  ) {
                    instance.hide();
                  }
                });
              },
            };
          },
        };
        const hideOnEsc = {
          /* https://atomiks.github.io/tippyjs/v6/plugins/#hideonesc */
          name: 'hideOnEsc',
          defaultValue: true,
          fn({hide}) {
            function onKeyDown(event) {
              if (event.keyCode === 27) {
                hide();
              }
            }

            return {
              onShow() {
                document.addEventListener('keydown', onKeyDown);
              },
              onHide() {
                document.removeEventListener('keydown', onKeyDown);
              }
            };
          }
        };

        // https://atomiks.github.io/tippyjs/v6/all-props/
        tippy(buttonDropdown, {
          placement: 'bottom-start',
          inlinePositioning: true,
          content: tooltip,
          allowHTML: true, // Make sure you are sanitizing any user data if rendering HTML to prevent XSS attacks.
          trigger: 'click',
          hideOnClick: true,
          interactive: true,
          arrow: false,
          offset: [0, -10],
          theme: '',
          plugins: [hideOnEsc,hideOnPopperBlur],
          role: roleCustom ? roleCustom : false,
          aria: {
            content: 'auto'
          },
          onCreate(instance) {
            if(ariaHasPopup){
              instance.reference.setAttribute('aria-haspopup', ariaHasPopup);
            }
          },
          onShown(instance) {
            aria.Utils.focusFirstDescendant(tooltip);
            if(ariaLabel){
              instance.popper.firstElementChild.setAttribute('aria-label', ariaLabel);
            }
            if(ariaModal){
              instance.popper.firstElementChild.setAttribute('aria-modal', ariaModal);
            }
          }
        });
      }
    }
  }
}

export function listboxComponent(aria) {
  listbox(aria);
  const modules = document.querySelectorAll('[data-module]');
  let activeButton;
  for (const item in modules)
    if (modules.hasOwnProperty(item)) {
      const moduleValue = modules[item].getAttribute('data-module');

      if (moduleValue == 'c-listbox') {
        const buttonListbox = modules[item].querySelector('[data-module = "c-listbox-button"]');
        const tooltip = modules[item].querySelector('[data-module = "c-listbox-tooltip"]');
        const list = modules[item].querySelector('[data-module = "c-listbox-list"]');
        if (buttonListbox && tooltip) {
          const listbox = new aria.Listbox(list);
          const hideOnClick = {
            name: 'hideOnClick',
            defaultValue: true,
            fn(instance) {
              return {
                onCreate() {
                  buttonListbox.addEventListener("click", (event) => {
                    if (!buttonListbox.classList.contains('open')) {
                      instance.show();
                      list.focus()
                      buttonListbox.classList.add('open');
                    } else {
                      list.blur();
                      instance.hide();
                    }
                  });
                },
              };
            },
          };
          const hideOnPopperBlur = {
            /* https://atomiks.github.io/tippyjs/v6/plugins/#hideonpopperblur */
            name: 'hideOnPopperBlur',
            defaultValue: true,
            fn(instance) {
              return {
                onCreate() {
                  instance.popper.addEventListener('focusout', (event) => {
                    if (
                      instance.props.hideOnPopperBlur &&
                      event.relatedTarget &&
                      !instance.popper.contains(event.relatedTarget)
                    ) {
                      instance.hide();
                    }
                  });
                },
              };
            },
          };
          const hideOnEscOrEnter = {
            /* https://atomiks.github.io/tippyjs/v6/plugins/#hideonesc */
            name: 'hideOnEsc',
            defaultValue: true,
            fn({ hide }) {
              function onKeyDown(event) {
                if ((event.keyCode === 27) || (event.keyCode === 13)) {
                  hide();
                }
              }

              return {
                onShow() {
                  document.addEventListener('keydown', onKeyDown);
                },
                onHide() {
                  document.removeEventListener('keydown', onKeyDown);
                }
              };
            }
          };

          // https://atomiks.github.io/tippyjs/v6/all-props/
          tippy(buttonListbox, {
            placement: 'bottom-start',
            inlinePositioning: true,
            content: tooltip,
            allowHTML: true, // Make sure you are sanitizing any user data if rendering HTML to prevent XSS attacks.
            trigger: 'manual',
            interactive: true,
            arrow: false,
            offset: [0, -10],
            theme: '',
            plugins: [hideOnEscOrEnter, hideOnPopperBlur, hideOnClick],
            role: false,
            aria: {
              content: 'auto'
            },
            onMount(instance) {
              if (list.querySelectorAll('[aria-selected="true"]')[0]) {
                listbox.focusItem(list.querySelectorAll('[aria-selected="true"]')[0]);
              }
            },
            onHidden(instance) {
              buttonListbox.classList.remove('open');
            }
          });

          listbox.setHandleFocusChange(function(evt) {
            if ((!list.hasAttribute('aria-multiselectable')) && (buttonListbox.getAttribute('data-change') == 'change')) {
              buttonListbox.firstElementChild.innerHTML = evt.innerHTML;
            }
          });

          const getMultiSelectable = buttonListbox.getAttribute('aria-labelledby');
          const allowMultiple = getMultiSelectable.includes('is-multiselectable-label');

          if (!allowMultiple) {
            list.addEventListener("click", (event) => {
              buttonListbox.click();
            })
          }
        }
      }
    }
}

export function linksListComponent(aria) {
  LinksList(aria);

  const modules = document.querySelectorAll('[data-module="c-links-list"]');
  [...modules].forEach((module) => {
    aria.linksListInit(module);
  });
}

export function menubarComponent(aria) {
  PopupMenuItemAction(aria);
  PopupMenuAction(aria);
  MenubarItemAction(aria);
  MenubarAction(aria);
  const modules = document.querySelectorAll('[data-module="c-menubar"]');
  [...modules].forEach((module) => {
    const menubar = new aria.MenubarAction(module);
    menubar.init(null);
  });
}

export function tableAdvancedComponent(aria) {
  utils(aria);
  dataGrid(aria);
  const modules = document.querySelectorAll('[data-module="c-table-advanced"]');
  [...modules].forEach((module) => {
    const grid = new aria.Grid(module);
  });
}

export function tabsComponent(aria) {
  tabs(aria);
  const modules = document.querySelectorAll('[data-module="c-tabs"]');
  [...modules].forEach((module) => {
    aria.tabsInit(module);
  });
}

export function tooltipComponent(aria) {
  const modules = document.querySelectorAll('[data-module]');
  for (const item in modules)
    if (modules.hasOwnProperty(item)) {
      const moduleValue = modules[item].getAttribute('data-module');


      if (moduleValue == 'c-tooltip') {
        const tooltipButton = modules[item].querySelector('[data-module = "c-tooltip-button"]');
        const ariaContent = tooltipButton.matches('[data-type = "c-tooltip-button-complex"]') ? 'describedby' : 'labelledby';
        const tooltipTooltip = modules[item].querySelector('[data-module = "c-tooltip-tooltip"]');
        if (tooltipButton && tooltipTooltip) {
          const hideOnPopperBlur = {
            /* https://atomiks.github.io/tippyjs/v6/plugins/#hideonpopperblur */
            name: 'hideOnPopperBlur',
            defaultValue: true,
            fn(instance) {
              return {
                onCreate() {
                  instance.popper.addEventListener('focusout', (event) => {
                    if (
                      instance.props.hideOnPopperBlur &&
                      event.relatedTarget &&
                      !instance.popper.contains(event.relatedTarget)
                    ) {
                      instance.hide();
                    }
                  });
                },
              };
            },
          };
          const hideOnEsc = {
            /* https://atomiks.github.io/tippyjs/v6/plugins/#hideonesc */
            name: 'hideOnEsc',
            defaultValue: true,
            fn({ hide }) {
              function onKeyDown(event) {
                if (event.keyCode === 27) {
                  hide();
                }
              }

              return {
                onShow() {
                  document.addEventListener('keydown', onKeyDown);
                },
                onHide() {
                  document.removeEventListener('keydown', onKeyDown);
                }
              };
            }
          };

          // https://atomiks.github.io/tippyjs/v6/all-props/
          tippy(tooltipButton, {
            placement: 'top',
            inlinePositioning: true,
            content: tooltipTooltip,
            allowHTML: true, // Make sure you are sanitizing any user data if rendering HTML to prevent XSS attacks.
            trigger: 'mouseenter focus',
            hideOnClick: false,
            theme: '',
            plugins: [hideOnEsc, hideOnPopperBlur],
            role: 'tooltip',
            interactive: true,
            aria: {
              content: ariaContent
            }
          });
        }
      }
    }
}

export function treeComponent(aria) {
  Treeitem(aria);
  Tree(aria);
  const modules = document.querySelectorAll('[data-module]');
  for (const item in modules)
    if (modules.hasOwnProperty(item)) {
      const moduleValue = modules[item].getAttribute('data-module');

      if (moduleValue == 'c-tree') {
        const tree = new aria.Tree(modules[item]);
        tree.init();
      }

      if (moduleValue == 'c-tree__item') {
        modules[item].addEventListener('click', function(event) {
          event.stopPropagation();
        });
      }
    }
}

export function notificationComponent(aria) {
  Notification(aria);
  const modules = document.querySelectorAll('[data-module="c-notification"]');
  [...modules].forEach((module) => {
    aria.notificationInit(module);
  });
}

export function radioButtonComponent(aria) {
  RadioButton(aria);
}

export function checkBoxComponent(aria) {
  CheckBox(aria);
}

export function MenuVerticalComponent(aria) {
  MenuVertical(aria);
  const modules = document.querySelectorAll('[data-module="c-menu-vertical"]');
  [...modules].forEach((module) => {
    const menuVertical = new aria.MenuVertical(module);
    menuVertical.init(null);
  });
}

export function MenuHorizontalComponent(aria) {
  MenuHorizontal(aria);
  const modules = document.querySelectorAll('[data-module="c-menu-horizontal"]');
  [...modules].forEach((module) => {
    const MenuHorizontal = new aria.MenuHorizontal(module);
    MenuHorizontal.init(null);
  });
}

export function MenuNavigationComponent(aria) {
  MenuNavigation(aria);
  const modules = document.querySelectorAll('[data-module="c-menu-navigation"]');
  [...modules].forEach((module) => {
    const menuNavigation = new aria.MenuNavigation(module);
    menuNavigation.init(null);
  });
}

export function HeaderNavigationComponent(aria) {
  HeaderNavigation(aria);
  const modules = document.querySelectorAll('[data-module="c-header-navigation"]');
  [...modules].forEach((module) => {
    const headerNavigation = new aria.HeaderNavigation(module);
    headerNavigation.init(null);
  });
}

export function NavComponent(aria) {
  Nav(aria);
  const modules = document.querySelectorAll('[data-module="c-nav"]');
  [...modules].forEach((module) => {
    const nav = new aria.Nav(module);
    nav.init(null);
  });
}

export function DatepickerComponent(aria) {
  const modules = document.querySelectorAll('[data-module="c-datepicker"]');
  [...modules].forEach((module) => {
    const inputElement = module.getElementsByTagName('input')[0];
    if (inputElement) {
      const dropdownId = inputElement.getAttribute('id');
      const datepickerDropdown = inputElement.parentNode.querySelector('[data-module="c-dropdown-button"]');
      let datepickerCalendar = null;
      let isOpen = false;

      // Variable to store the initial value only when the calendar opens
      let storedInitialValue = '';
      let storedInitialFormat = ''; // 'iso' or 'dd-mm-yyyy'

      const focusCalendar = () => {
        const datepickerSelect = module.querySelector(':is(select)');
        const datepickerCalendar = module.querySelector(':is(calendar-date, calendar-multi, calendar-range)');

        // Store the initial value only when the calendar opens
        if (!storedInitialValue) {
          storedInitialValue = datepickerCalendar === null ? "" : datepickerCalendar.value;
          // Detect initial format
          storedInitialFormat = storedInitialValue.includes('-') &&
            storedInitialValue.split('-')[0].length === 4 ? 'iso' : 'dd-mm-yyyy';
        }

        requestAnimationFrame(() => {
          if (datepickerCalendar != undefined) {
            datepickerCalendar.focus();
            datepickerCalendar.addEventListener('change', (e) => {
              inputElement.value = formatDateToDDMMYYYY(e.target.value);
            });
          }

          if (datepickerSelect != undefined) {
            datepickerSelect.addEventListener('change', function() {
              const selectedValue = this.value;
              datepickerCalendar.min = selectedValue + "-01-01";
              datepickerCalendar.max = selectedValue + "-12-31";
              datepickerCalendar.focusedDate = datepickerCalendar.min;
            });
            datepickerSelect.focus();
          }

          const datepickerCancel = module.querySelector('#' + dropdownId + '-cancel');
          const datepickerSubmit = module.querySelector('#' + dropdownId + '-submit');

          if (datepickerCancel != undefined) {
            datepickerCancel.onclick = function() {
              // Restore values according to original format
              if (storedInitialFormat === 'iso') {
                datepickerCalendar.value = storedInitialValue;
                inputElement.value = formatDateToDDMMYYYY(storedInitialValue);
              } else {
                datepickerCalendar.value = convertToISOFormat(storedInitialValue);
                inputElement.value = storedInitialValue;
              }

              datepickerCalendar.tentative = "";
              closeDropdown();
            }
          }

          if (datepickerSubmit != undefined) {
            datepickerSubmit.onclick = function() {
              // On submit, clear stored value for next opening
              storedInitialValue = '';
              storedInitialFormat = '';
              closeDropdown();
            }
          }
        });
      }

      const closeDropdown = () => {
        setTimeout(() => {
          isOpen = false;
          inputElement.focus();
        }, 300);
      };

      const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
          if (mutation.type === 'attributes' && mutation.attributeName === 'aria-expanded') {
            const currentExpandedValue = mutation.target.getAttribute('aria-expanded');
            if (currentExpandedValue !== isOpen) {
              const datepickerTimeout = setTimeout(focusCalendar, 500);
              isOpen = currentExpandedValue;
              if (!isOpen) {
                clearTimeout(datepickerTimeout);
                closeDropdown();
              }
            }
          }
        });
      });

      observer.observe(datepickerDropdown, { attributes: true, attributeFilter: ['aria-expanded'] });
    } else {
      const datepickerSelect = module.querySelector(':is(select)');
      const datepickerCalendar = module.querySelector(':is(calendar-date, calendar-multi, calendar-range)');
      if (datepickerSelect != undefined) {
        datepickerSelect.addEventListener('change', function() {
          const selectedValue = this.value;
          datepickerCalendar.min = selectedValue + "-01-01";
          datepickerCalendar.max = selectedValue + "-12-31";
          datepickerCalendar.focusedDate = datepickerCalendar.min;
        });
      }
    }
  });
}

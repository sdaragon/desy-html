export function alert(aria) {
    aria.Alert = function (alertId, focusAfterClosed, focusFirst) {
      this.alertNode = document.getElementById(alertId);
      if (this.alertNode === null) {
        throw new Error('No element found with id="' + alertId + '".');
      }


      document.getElementById(alertId).innerHTML = document.getElementById('template-' + alertId).innerHTML;

      if (typeof focusAfterClosed === 'string') {
        this.focusAfterClosed = document.getElementById(focusAfterClosed);
      }
      else if (typeof focusAfterClosed === 'object') {
        this.focusAfterClosed = focusAfterClosed;
      }
      else {
        throw new Error(
          'the focusAfterClosed parameter is required for the aria.Alert constructor.');
      }

      if (typeof focusFirst === 'string') {
        this.focusFirst = document.getElementById(focusFirst);
      }
      else if (typeof focusFirst === 'object') {
        this.focusFirst = focusFirst;
      }
      else {
        this.focusFirst = null;
      }

      if (this.focusFirst) {
        this.focusFirst.focus();
      }

      this.lastFocus = document.activeElement;
  }; // end Alert constructor

  window.openAlert = function (alertId, focusAfterClosed, focusFirst) {
    var alert = new aria.Alert(alertId, focusAfterClosed, focusFirst);
  };
}
export function Toggle(aria) {

  aria.toggleInit = function (domNode) {
    this.rootEl = domNode;
    this.buttonEl = this.rootEl.querySelector('.c-toggle__button');
    this.buttonEl.addEventListener('click', this.toggle.bind(this));
    if(this.buttonEl.getAttribute('aria-pressed') === 'true' || this.buttonEl.getAttribute('aria-checked') === 'true') {
      this.buttonEl.click()
    }
  };

  aria.toggle = function (event) {
    const { target } = event
    target.classList.toggle('c-toggle--is-opened')

    this.ariaAttribute(target)
  }

  aria.ariaAttribute = function (target) {
    if(target.getAttribute('aria-checked')) {
      const toggleIsOpened = target.classList.contains('c-toggle--is-opened') ? true : false
      target.setAttribute('aria-checked', toggleIsOpened);
    }

    if(target.getAttribute('aria-pressed')) {
      const toggleIsOpened = target.classList.contains('c-toggle--is-opened') ? true : false
      target.setAttribute('aria-pressed', toggleIsOpened);
    }

    if(target.getAttribute('aria-expanded')) {
      const toggleIsOpened = target.classList.contains('c-toggle--is-opened') ? true : false
      target.setAttribute('aria-expanded', toggleIsOpened);
    }
  }

  function handleToggle(element, toggle) {
    if(toggle){
      element.classList.add('c-toggle--is-opened')
    } else {
      element.classList.remove('c-toggle--is-opened')
    }

    aria.ariaAttribute(element)
  }

  window.activateItemToggle = function (element, toggle) {
    const toggleElement = document.getElementById(element);
    if (toggleElement) {
      handleToggle(toggleElement, toggle)
      return [toggleElement, toggle];
    } else {
      console.log('There is no toggle with this id in the document.');
      return null;
    }
  };

  const toggles = document.querySelectorAll('[data-module="c-toggle"]');
  toggles.forEach((toggleElement) => {
    aria.toggleInit(toggleElement);
  });
}

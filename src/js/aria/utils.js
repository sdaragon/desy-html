export function utils(aria) {
  /**
   * @namespace aria
   */

  aria.Utils = aria.Utils || {};

  /**
   * @desc
   *  Key code constants
   */
  aria.KeyCode = {
    BACKSPACE: 8,
    TAB: 9,
    RETURN: 13,
    ESC: 27,
    SPACE: 32,
    PAGE_UP: 33,
    PAGE_DOWN: 34,
    END: 35,
    HOME: 36,
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    DELETE: 46
  };


  // Polyfill src https://developer.mozilla.org/en-US/docs/Web/API/Element/matches
  aria.Utils.matches = function(element, selector) {
    if (!Element.prototype.matches) {
      Element.prototype.matches =
        Element.prototype.matchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector ||
        Element.prototype.oMatchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        function(s) {
          var matches = element.parentNode.querySelectorAll(s);
          var i = matches.length;
          while (--i >= 0 && matches.item(i) !== this) {}
          return i > -1;
        };
    }

    return element.matches(selector);
  };

  aria.Utils.remove = function(item) {
    if (item.remove && typeof item.remove === 'function') {
      return item.remove();
    }
    if (item.parentNode &&
      item.parentNode.removeChild &&
      typeof item.parentNode.removeChild === 'function') {
      return item.parentNode.removeChild(item);
    }
    return false;
  };

  aria.Utils.isFocusable = function(element) {
    if (element.tabIndex > 0 || (element.tabIndex === 0 && element.getAttribute('tabIndex') !== null)) {
      return true;
    }

    if (element.disabled) {
      return false;
    }

    switch (element.nodeName) {
      case 'A':
        return !!element.href && element.rel != 'ignore';
      case 'INPUT':
        return element.type != 'hidden' && element.type != 'file';
      case 'BUTTON':
      case 'SELECT':
      case 'TEXTAREA':
        return true;
      default:
        return false;
    }
  };

  aria.Utils.getAncestorBySelector = function(element, selector) {
    if (!aria.Utils.matches(element, selector + ' ' + element.tagName)) {
      // Element is not inside an element that matches selector
      return null;
    }

    // Move up the DOM tree until a parent matching the selector is found
    var currentNode = element;
    var ancestor = null;
    while (ancestor === null) {
      if (aria.Utils.matches(currentNode.parentNode, selector)) {
        ancestor = currentNode.parentNode;
      } else {
        currentNode = currentNode.parentNode;
      }
    }

    return ancestor;
  };

  aria.Utils.hasClass = function(element, className) {
    return (new RegExp('(\\s|^)' + className + '(\\s|$)')).test(element.className);
  };

  aria.Utils.addClass = function(element, className) {
    if (!aria.Utils.hasClass(element, className)) {
      element.className += ' ' + className;
    }
  };

  aria.Utils.removeClass = function(element, className) {
    var classRegex = new RegExp('(\\s|^)' + className + '(\\s|$)');
    element.className = element.className.replace(classRegex, ' ').trim();
  };

  aria.Utils.bindMethods = function(object /* , ...methodNames */ ) {
    var methodNames = Array.prototype.slice.call(arguments, 1);
    methodNames.forEach(function(method) {
      object[method] = object[method].bind(object);
    });
  };
}

// Function to convert ISO date(s) to DD-MM-YYYY format
export function formatDateToDDMMYYYY(isoDate) {
  if (!isoDate) return '';
  try {
    // Case 1: Multiple dates separated by space
    if (isoDate.includes(' ')) {
      return isoDate
        .split(' ')
        .map(date => formatSingleDate(date))
        .join(' ');
    }
    // Case 2: Date range separated by /
    if (isoDate.includes('/')) {
      const [startDate, endDate] = isoDate.split('/');
      return `${formatSingleDate(startDate)}/${formatSingleDate(endDate)}`;
    }
    // Case 3: Single date
    return formatSingleDate(isoDate);
  } catch (error) {
    console.error('Error formatting date:', error);
    return '';
  }
}

// Helper function to format a single date
export function formatSingleDate(isoDate) {
  const date = new Date(isoDate);
  if (isNaN(date.getTime())) {
    return '';
  }
  const day = String(date.getDate()).padStart(2, '0');
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const year = date.getFullYear();
  return `${day}-${month}-${year}`;
}

// Function to convert DD-MM-YYYY format to ISO
export function convertToISOFormat(ddmmyyyyDate) {
  if (!ddmmyyyyDate) return '';
  try {
    // For multiple dates
    if (ddmmyyyyDate.includes(' ')) {
      return ddmmyyyyDate
        .split(' ')
        .map(date => convertSingleDateToISO(date))
        .join(' ');
    }
    // For date ranges
    if (ddmmyyyyDate.includes('/')) {
      const [startDate, endDate] = ddmmyyyyDate.split('/');
      return `${convertSingleDateToISO(startDate)}/${convertSingleDateToISO(endDate)}`;
    }
    // For single date
    return convertSingleDateToISO(ddmmyyyyDate);
  } catch (error) {
    console.error('Error converting to ISO format:', error);
    return '';
  }
}

// Helper function to convert a single date to ISO
function convertSingleDateToISO(ddmmyyyyDate) {
  if (!ddmmyyyyDate) return '';
  const [day, month, year] = ddmmyyyyDate.split('-');
  return `${year}-${month}-${day}`;
}

export function CheckBox(aria) {

  class CheckBox {
    constructor(domNode) {
      this.rootEl = domNode;
      this.rootEl.querySelectorAll('.c-checkboxes__conditional').forEach(item => {
        item.addEventListener('click', this.toggleConditional.bind(this))
        this.toggleConditionalInit(item)
      })

      this.rootEl.querySelectorAll('.c-checkboxes__indeterminate-active').forEach(item => {
        this.toggleIndeterminate(item, true)
      })
    }

    toggleConditionalInit(item) {
      const getInput = item.querySelector('input');
      if(getInput.checked) {
        item.classList.remove('c-checkboxes__conditional-hidden');
        item.classList.add('c-checkboxes__conditional-active');
      } else {
        item.classList.add('c-checkboxes__conditional-hidden');
        item.classList.remove('c-checkboxes__conditional-active');
      }
    }

    toggleConditional(event) {
      const { target: { nodeName, checked, type }, target } = event
      if(nodeName === 'INPUT' && type === 'checkbox' && checked) {
        target.parentElement.parentElement.parentElement.classList.remove('c-checkboxes__conditional-hidden');
        target.parentElement.parentElement.parentElement.classList.add('c-checkboxes__conditional-active');
      } else if (nodeName === 'INPUT' && type === 'checkbox' && !checked) {
        target.parentElement.parentElement.parentElement.classList.add('c-checkboxes__conditional-hidden');
        target.parentElement.parentElement.parentElement.classList.remove('c-checkboxes__conditional-active');
      }
    }

    toggleIndeterminate(event) {
      const selectInput = event.querySelector('input');
      selectInput.readOnly = true
      selectInput.indeterminate = true
    }
  }

  const checkBoxesElements = document.querySelectorAll('[data-module="c-checkboxes"]');
  checkBoxesElements.forEach((checkBoxElement) => {
    if (checkBoxElement.querySelector('.c-checkboxes__conditional') || checkBoxElement.querySelector('.c-checkboxes__indeterminate-active')) {
      new CheckBox(checkBoxElement);
    }
  });

}

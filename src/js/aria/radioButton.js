export function RadioButton(aria) {

  class RadioButton {
    constructor(domNode) {
      this.rootEl = domNode;
      this.rootEl.querySelectorAll('.c-radios__conditional-active input[type="radio"], .c-radios__conditional-hidden input[type="radio"]').forEach(item => {
        item.addEventListener('click', this.toggleConditional.bind(this))
      })
    }

    toggleConditional(event) {
      const { target: { nodeName, type }, target } = event
      const targetParent = target.parentElement.parentElement.parentElement.parentElement;
      const getChildrens = targetParent.querySelectorAll('.c-radios__conditional');

      if(targetParent && targetParent.classList.contains('c-radios__conditional-active')) {
        return;
      }

      if(getChildrens.length > 0) {
        getChildrens.forEach(item => {
          if(item.querySelector('input[type="radio"]').checked) {
            item.classList.remove('c-radios__conditional-hidden')
            item.classList.add('c-radios__conditional-active')
          }
        })
      }

      if(type === 'radio' && nodeName === 'INPUT') {
        this.rootEl.querySelectorAll('.c-radios__conditional-active, .c-radios__conditional-hidden').forEach(item => {
          if(item.querySelector('input[type="radio"]').checked){
            item.classList.remove('c-radios__conditional-hidden')
            item.classList.add('c-radios__conditional-active')
          } else {
            item.classList.remove('c-radios__conditional-active')
            item.classList.add('c-radios__conditional-hidden')
          }
        })
      }
    }
  }

  const radioElements = document.querySelectorAll('[data-module="c-radios"]');
  radioElements.forEach((radioElement) => {
    if (radioElement.querySelector('.c-radios__conditional')) {
      new RadioButton(radioElement);
    }
  });

}

export function MenuHorizontal(aria) {
  (function () {
    aria.MenuHorizontal = function (domNode) {
      this.domNode = domNode;
    };

    aria.MenuHorizontal.prototype.init = function () {
      const menuItems = this.domNode.querySelectorAll('a');
      const getItemsFromMenu = Array.from(menuItems);

      getItemsFromMenu.forEach((button) => {
        if(button.classList.contains('c-menu-horizontal__active')) {
          this.activateElement(button.id);
        }
      })
    };

    aria.MenuHorizontal.prototype.activateElement = function (elementActive) {
      this.domNode.querySelectorAll('a').forEach((element) => {
        if (element.id === elementActive) {
          this.wrapActiveElement(element);
        } else {
          this.deactivateElement(element);
        }
      });
    };

    aria.MenuHorizontal.prototype.wrapActiveElement = function (elementActive) {
      if (!elementActive.getAttribute('aria-current')) {
        elementActive.setAttribute('aria-current', 'page');
        elementActive.classList.add('c-menu-horizontal__active');
        elementActive.innerHTML = `<strong class="font-bold">${elementActive.innerHTML}</strong>`;
      }
    };

    aria.MenuHorizontal.prototype.deactivateElement = function (elementDeactivated) {
      elementDeactivated.removeAttribute('aria-current');
      elementDeactivated.classList.remove('c-menu-horizontal__active');
      if(elementDeactivated.querySelector('strong')) {
        const replaceStrong = elementDeactivated.innerHTML.replace('<strong class="font-bold">', '').replace('<strong/>', '');
        elementDeactivated.innerHTML = `${replaceStrong}`;
      }
    };

    window.activateItemMenuHorizontal = function (menuId, activeItemId) {
      const menu = document.getElementById(menuId);
      if (menu) {
        const activeItem = document.querySelector(`#${menuId} #${activeItemId}`);
        if (activeItem) {
          const menuInstance = new aria.MenuHorizontal(menu);
          menuInstance.activateElement(activeItemId);
          return [menu, activeItem];
        } else {
          console.log('There is no element with this id in the menu.');
          return null;
        }
      } else {
        console.log('There is no menu with this id in the document.');
        return null;
      }
    };
  }());
}

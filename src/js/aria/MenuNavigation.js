export function MenuNavigation(aria) {
  /*
  *   This content is licensed according to the W3C Software License at
  *   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
  *
  *   Supplemental JS for the disclosure menu keyboard behavior
  */

  aria.MenuNavigation = function (domNode) {
    this.rootNode = domNode;
    this.controlledNodes = [];
    this.openIndex = null;
    this.useArrowKeys = true;
    this.topLevelNodes = [...this.rootNode.querySelectorAll('a.c-menu-navigation__button, button[aria-expanded][aria-controls]')];

    this.anchorChildren = [...this.rootNode.querySelectorAll('nav ul[aria-labelledby] li a')];
  };

  aria.MenuNavigation.prototype.init = function () {
    const menuItems = this.rootNode.querySelectorAll('a');
    const getItemsFromMenu = Array.from(menuItems);

    if(getItemsFromMenu.some(item => item.classList.contains('c-menu-navigation__button--has-selection'))) {
      getItemsFromMenu.forEach((button) => {
        if(button.classList.contains('c-menu-navigation__button--has-selection')) {
          this.activateElement(button.id);
        }
      })
    }

    if(this.rootNode.querySelectorAll('.c-menu-navigation__sub--list')) {
      const subItems = this.rootNode.querySelectorAll('.c-menu-navigation__sub--list li a');
      [...subItems].forEach((element) => {
        if(element.getAttribute("aria-current") === "page"){
          const getElement = element.parentElement.parentElement.parentElement.parentElement.querySelector('button');
          getElement.classList.add('c-menu-navigation__button--primary', 'c-menu-navigation__button--has-selection');
          getElement.firstChild.innerHTML = `<strong class="font-bold">${getElement.textContent}</strong>`;
          element.innerHTML = `<strong class="font-bold">${element.textContent}</strong>`;
        }
      })
    }

    this.topLevelNodes.forEach((node) => {
      // handle button + menu
      if (
        node.tagName.toLowerCase() === 'button' &&
        node.hasAttribute('aria-controls')
      ) {
        const menu = node.parentNode.querySelector('ul');
        if (menu) {
          // save ref controlled menu
          this.controlledNodes.push(menu);

          // collapse menus
          node.setAttribute('aria-expanded', 'false');
          this.toggleMenu(menu, false);

          // attach event listeners
          menu.addEventListener('keydown', this.onMenuKeyDown.bind(this));
          node.addEventListener('click', this.onButtonClick.bind(this));
          node.addEventListener('keydown', this.onButtonKeyDown.bind(this));
        }
      }
      // handle links
      else {
        this.controlledNodes.push(null);
        node.addEventListener('keydown', this.onLinkKeyDown.bind(this));
      }
    });

    this.rootNode.addEventListener('focusout', this.onBlur.bind(this));
    this.anchorChildren.forEach((element) => {
      element.addEventListener('click', this.onBlur.bind(this));
    })
  };

  aria.MenuNavigation.prototype.activateElement = function (elementActive) {
    const getAllLiElements = this.rootNode.querySelectorAll('li');
    [...getAllLiElements].forEach((element) => {
      const getElement = element.querySelector('a');
      if (getElement.id === elementActive) {
        this.wrapActiveElement(getElement);
      } else {
        this.deactivateElement(getElement);
      }
    });
  };

  aria.MenuNavigation.prototype.activateSubElement = function (menuId, elementActive) {
    const getAllLinks = this.rootNode.querySelectorAll(`#${menuId} ul li li a`);
    [...getAllLinks].forEach((link) => {
      if(link.id === elementActive) {
        link.setAttribute('aria-current', 'page');
        link.innerHTML = `<strong class="font-bold">${link.textContent}</strong>`;
      } else {
        const getElementParent = link.parentElement.parentElement.parentElement.parentElement.querySelector('button');
        getElementParent.classList.remove('c-menu-navigation__button--primary', 'c-menu-navigation__button--has-selection');
        getElementParent.firstChild.innerHTML = `${getElementParent.textContent}`;
        link.removeAttribute('aria-current');
        link.innerHTML = `${link.textContent}`;
      }
    });
    [...getAllLinks].forEach((link) => {
      if(link.getAttribute("aria-current") === "page"){
        const getElementParent = link.parentElement.parentElement.parentElement.parentElement.querySelector('button');
        getElementParent.classList.add('c-menu-navigation__button--primary', 'c-menu-navigation__button--has-selection');
        getElementParent.firstChild.innerHTML = `<strong class="font-bold">${getElementParent.textContent}</strong>`;
      }
    });
  };

  aria.MenuNavigation.prototype.wrapActiveElement = function (elementActive) {
    elementActive.setAttribute('aria-current', 'page');
    elementActive.classList.add('c-menu-navigation__button--primary', 'c-menu-navigation__button--has-selection');
    elementActive.innerHTML = `<strong class="font-bold">${elementActive.textContent}</strong>`;
  };

  aria.MenuNavigation.prototype.deactivateElement = function (elementDeactivated) {
    elementDeactivated.removeAttribute('aria-current');
    elementDeactivated.classList.remove('c-menu-navigation__button--primary', 'c-menu-navigation__button--has-selection');
    elementDeactivated.innerHTML = elementDeactivated.textContent;
  };

  window.activateItemMenuNavigation = function (menuId, activeItemId) {
    const menu = document.getElementById(menuId);
    if (menu) {
      const activeItem = document.querySelector(`#${menuId} #${activeItemId}`);
      if (activeItem) {
        const menuInstance = new aria.MenuNavigation(menu);
        menuInstance.activateElement(activeItemId);
        return [menu, activeItem];
      } else {
        console.log('There is no element with this id in the menu.');
        return null;
      }
    } else {
      console.log('There is no menu with this id in the document.');
      return null;
    }
  };

  window.activateSubItemMenuNavigation = function (menuId, activeSubItemId) {
    const menu = document.getElementById(menuId);
    if (menu) {
      const activeSubItem = document.querySelector(`#${menuId} #${activeSubItemId}`);
      if (activeSubItem) {
        const menuInstance = new aria.MenuNavigation(menu);
        menuInstance.activateSubElement(menuId, activeSubItemId);
        return [menu, activeSubItem];
      } else {
        console.log('There is no element with this id in the menu.');
        return null;
      }
    } else {
      console.log('There is no menu with this id in the document.');
      return null;
    }
  };

  aria.MenuNavigation.prototype.controlFocusByKey = function (keyboardEvent, nodeList, currentIndex) {
    switch (keyboardEvent.key) {
      case 'ArrowUp':
      case 'ArrowLeft':
        keyboardEvent.preventDefault();
        if (currentIndex > -1) {
          var prevIndex = Math.max(0, currentIndex - 1);
          if(!nodeList[prevIndex].classList.contains('c-menu-navigation__button--disabled')) {
            nodeList[prevIndex].focus();
          }
        }
        break;
      case 'ArrowDown':
      case 'ArrowRight':
        keyboardEvent.preventDefault();
        if (currentIndex > -1) {
          var nextIndex = Math.min(nodeList.length - 1, currentIndex + 1);
          if(!nodeList[nextIndex].classList.contains('c-menu-navigation__button--disabled')) {
            nodeList[nextIndex].focus();
          }
        }
        break;
      case 'Home':
        keyboardEvent.preventDefault();
        nodeList[0].focus();
        break;
      case 'End':
        keyboardEvent.preventDefault();
        nodeList[nodeList.length - 1].focus();
        break;
    }
  };

  // public function to close open menu
  aria.MenuNavigation.prototype.onBlur = function (event) {
    var menuContainsFocus = this.rootNode.contains(event.relatedTarget);
    if (!menuContainsFocus && this.openIndex !== null) {
      this.toggleExpand(this.openIndex, false);
    }
  };

  aria.MenuNavigation.prototype.onButtonClick = function (event) {
    var button = event.target;
    var buttonIndex = this.topLevelNodes.indexOf(button);
    var buttonExpanded = button.getAttribute('aria-expanded') === 'true';
    this.toggleExpand(buttonIndex, !buttonExpanded);
  };

  aria.MenuNavigation.prototype.onButtonKeyDown = function (event) {
    var targetButtonIndex = this.topLevelNodes.indexOf(document.activeElement);

    // close on escape
    if (event.key === 'Escape') {
      this.toggleExpand(this.openIndex, false);
    }

    // move focus into the open menu if the current menu is open
    else if (
      this.useArrowKeys &&
      this.openIndex === targetButtonIndex &&
      event.key === 'ArrowDown'
    ) {
      event.preventDefault();
      this.controlledNodes[this.openIndex].querySelector('a').focus();
    }

    // handle arrow key navigation between top-level buttons, if set
    else if (this.useArrowKeys) {
      this.controlFocusByKey(event, this.topLevelNodes, targetButtonIndex);
    }
  };

  aria.MenuNavigation.prototype.onLinkKeyDown = function (event) {
    var targetLinkIndex = this.topLevelNodes.indexOf(document.activeElement);

    // handle arrow key navigation between top-level buttons, if set
    if (this.useArrowKeys) {
      this.controlFocusByKey(event, this.topLevelNodes, targetLinkIndex);
    }
  };

  aria.MenuNavigation.prototype.onMenuKeyDown = function (event) {
    if (this.openIndex === null) {
      return;
    }

    var menuLinks = Array.prototype.slice.call(
      this.controlledNodes[this.openIndex].querySelectorAll('a')
    );
    var currentIndex = menuLinks.indexOf(document.activeElement);

    // close on escape
    if (event.key === 'Escape') {
      this.topLevelNodes[this.openIndex].focus();
      this.toggleExpand(this.openIndex, false);
    }

    // handle arrow key navigation within menu links, if set
    else if (this.useArrowKeys) {
      this.controlFocusByKey(event, menuLinks, currentIndex);
    }
  };

  aria.MenuNavigation.prototype.toggleExpand = function (index, expanded) {
    // close open menu, if applicable
    if (this.openIndex !== index) {
      this.toggleExpand(this.openIndex, false);
    }

    // handle menu at called index
    if (this.topLevelNodes[index]) {
      this.openIndex = expanded ? index : null;
      this.topLevelNodes[index].setAttribute('aria-expanded', expanded);
      this.toggleMenu(this.controlledNodes[index], expanded);
    }
  };

  aria.MenuNavigation.prototype.toggleMenu = function (domNode, show) {
    if (domNode) {
      domNode.style.display = show ? 'block' : 'none';
    }
  };

  aria.MenuNavigation.prototype.updateKeyControls = function (useArrowKeys) {
    this.useArrowKeys = useArrowKeys;
  };

  window.addEventListener(
    'load',
    function () {
      var menus = document.querySelectorAll('.disclosure-nav');
      var disclosureMenus = [];

      for (var i = 0; i < menus.length; i++) {
        disclosureMenus[i] = new DisclosureNav(menus[i]);
      }

      // listen to arrow key checkbox
      var arrowKeySwitch = document.getElementById('arrow-behavior-switch');
      if (arrowKeySwitch) {
        arrowKeySwitch.addEventListener('change', function () {
          var checked = arrowKeySwitch.checked;
          for (var i = 0; i < disclosureMenus.length; i++) {
            disclosureMenus[i].updateKeyControls(checked);
          }
        });
      }

      // fake link behavior
      disclosureMenus.forEach((disclosureNav, i) => {
        var links = menus[i].querySelectorAll('[href="#mythical-page-content"]');
        var examplePageHeading = document.getElementById('mythical-page-heading');
        for (var k = 0; k < links.length; k++) {
          // The codepen export script updates the internal link href with a full URL
          // we're just manually fixing that behavior here
          links[k].href = '#mythical-page-content';

          links[k].addEventListener('click', (event) => {
            // change the heading text to fake a page change
            var pageTitle = event.target.innerText;
            examplePageHeading.innerText = pageTitle;

            // handle aria-current
            for (var n = 0; n < links.length; n++) {
              links[n].removeAttribute('aria-current');
            }
            event.target.setAttribute('aria-current', 'page');
          });
        }
      });
    },
    false
  );
}

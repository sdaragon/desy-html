export function Treeitem(aria) {
  /*
  *   This content is licensed according to the W3C Software License at
  *   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
  *
  *   File:   Treeitem.js
  *
  *   Desc:   Treeitem widget that implements ARIA Authoring Practices
  *           for a tree being used as a file viewer
  */

  /*
  *   @constructor
  *
  *   @desc
  *       Treeitem object for representing the state and user interactions for a
  *       treeItem widget
  *
  *   @param node
  *       An element with the role=tree attribute
  */

  aria.Treeitem = function (node, treeObj, group) {

    // Check whether node is a DOM element
    if (typeof node !== 'object') {
      return;
    }

    node.tabIndex = -1;
    this.tree = treeObj;
    this.groupTreeitem = group;
    this.domNode = node;
    this.label = node.textContent.trim();

    if(this.domNode.getAttribute("aria-current")) {
      var getLabel = this.domNode.querySelector('label')
      getLabel.setAttribute("aria-current", "page")
    }

    if (node.getAttribute('aria-label')) {
      this.label = node.getAttribute('aria-label').trim();
    }

    this.isExpandable = false;
    this.isVisible = false;
    this.inGroup = false;

    if (group) {
      this.inGroup = true;
    }

    var elem = node.firstElementChild;

    while (elem) {

      if (elem.tagName.toLowerCase() == 'ul') {
        elem.setAttribute('role', 'group');
        this.isExpandable = true;
        break;
      }

      elem = elem.nextElementSibling;
    }

    this.keyCode = Object.freeze({
      RETURN: 13,
      SPACE: 32,
      PAGEUP: 33,
      PAGEDOWN: 34,
      END: 35,
      HOME: 36,
      LEFT: 37,
      UP: 38,
      RIGHT: 39,
      DOWN: 40
    });
  };

  aria.Treeitem.prototype.init = function () {
    this.domNode.tabIndex = -1;

    if (!this.domNode.getAttribute('role')) {
      this.domNode.setAttribute('role', 'treeitem');
    }

    if(!this.domNode.getAttribute('disabled')){
      this.domNode.addEventListener('keydown', this.handleKeydown.bind(this));
      this.domNode.addEventListener('click', this.handleClick.bind(this));
      this.domNode.addEventListener('focus', this.handleFocus.bind(this));
      this.domNode.addEventListener('blur', this.handleBlur.bind(this));
    }

    if (!this.isExpandable) {
      this.domNode.addEventListener('mouseover', this.handleMouseOver.bind(this));
      this.domNode.addEventListener('mouseout', this.handleMouseOut.bind(this));
    }

    const getCheckboxes = document.querySelectorAll('.c-checkboxes__indeterminate-active');
    Array.from(getCheckboxes).forEach((checkbox) => {
      checkbox.readOnly = true
      checkbox.indeterminate = true
    });
  };

  aria.Treeitem.prototype.isExpanded = function () {

    if (this.isExpandable) {
      return this.domNode.getAttribute('aria-expanded') === 'true';
    }

    return false;

  };

  /* EVENT HANDLERS */

  aria.Treeitem.prototype.handleKeydown = function (event) {

    const targetElement = event.currentTarget;
    let flag = false;
    let char = event.key;
    const getCurrentCheckbox = targetElement.querySelector('input');
    const getAllChildrenOfTree = targetElement.parentElement.querySelectorAll('input');

    function isPrintableCharacter (str) {
      return str.length === 1 && str.match(/\S/);
    }

    function printableCharacter (item) {
      if (char == '*') {
        item.tree.expandAllSiblingItems(item);
        flag = true;
      }
      else {
        if (isPrintableCharacter(char)) {
          item.tree.setFocusByFirstCharacter(item, char);
          flag = true;
        }
      }
    }

    if (event.altKey || event.ctrlKey || event.metaKey) {
      return;
    }

    if (event.shift) {
      if (isPrintableCharacter(char)) {
        printableCharacter(this);
      }
    }
    else {
      switch (event.keyCode) {
        case this.keyCode.SPACE:
        case this.keyCode.RETURN:
          const typeOfInput = getCurrentCheckbox.type
          if(typeOfInput === 'radio') {
            for (let item of getAllChildrenOfTree) {
              item.checked = false
            }
          }
          getCurrentCheckbox.checked = getCurrentCheckbox.checked === true ? false : true
          flag = true;
          break;

        case this.keyCode.UP:
          this.tree.setFocusToPreviousItem(this);
          flag = true;
          break;

        case this.keyCode.DOWN:
          this.tree.setFocusToNextItem(this);
          flag = true;
          break;

        case this.keyCode.RIGHT:
          if (this.isExpandable) {
            if (this.isExpanded()) {
              this.tree.setFocusToNextItem(this);
            }
            else {
              this.tree.expandTreeitem(this);
            }
          }
          flag = true;
          break;

        case this.keyCode.LEFT:
          if (this.isExpandable && this.isExpanded()) {
            this.tree.collapseTreeitem(this);
            flag = true;
          }
          else {
            if (this.inGroup) {
              this.tree.setFocusToParentItem(this);
              flag = true;
            }
          }
          break;

        case this.keyCode.HOME:
          this.tree.setFocusToFirstItem();
          flag = true;
          break;

        case this.keyCode.END:
          this.tree.setFocusToLastItem();
          flag = true;
          break;

        default:
          if (isPrintableCharacter(char)) {
            printableCharacter(this);
          }
          break;
      }

    }

    if (flag) {
      event.stopPropagation();
      event.preventDefault();
    }
  };

  aria.Treeitem.prototype.handleClick = function (event) {
    if (this.isExpandable) {
      if (this.isExpanded()) {
        this.tree.collapseTreeitem(this);
      }
      else {
        this.tree.expandTreeitem(this);
      }
      event.stopPropagation();
    }
    else {
      this.tree.setFocusToItem(this);
    }
  };

  aria.Treeitem.prototype.handleFocus = function (event) {
    var node = this.domNode;
    if (this.isExpandable) {
      node = node.firstElementChild;
    }
    node.classList.add('c-tree__item--focus');
  };

  aria.Treeitem.prototype.handleBlur = function (event) {
    var node = this.domNode;
    if (this.isExpandable) {
      node = node.firstElementChild;
    }
    node.classList.remove('c-tree__item--focus');
  };

  aria.Treeitem.prototype.handleMouseOver = function (event) {
    event.currentTarget.classList.add('c-tree__item--hover');
  };

  aria.Treeitem.prototype.handleMouseOut = function (event) {
    event.currentTarget.classList.remove('c-tree__item--hover');
  };

  window.activateItemsTree = function (elementId, itemsIds, open = null) {
    const element = document.getElementById(elementId);
    if (element) {
      arrayOrSingleElement(elementId, itemsIds, open);
      return [elementId, itemsIds, open];
    } else {
      returnMessage()
    }
  };

  function arrayOrSingleElement(elementId, itemsIds, open) {
    if(typeof itemsIds === 'object' && open !== null) {
      itemsIds.forEach((item) => {
        const selectItem = document.querySelector(`#${elementId} #${item}`)
        if(selectItem) {
          activateElement(selectItem, open)
        } else {
          returnMessage()
        }
      })
    }

    if(typeof itemsIds !== 'object' && open !== null) {
      const getElement = document.querySelector(`nav #${elementId}`);
      const isTreeNavigation = getElement.hasAttribute('data-tree-navigation');
      const selectItem = document.querySelector(`#${elementId} #${itemsIds}`)
      if(selectItem) {
        activateElement(selectItem, open, isTreeNavigation)
      } else {
        returnMessage()
      }
    }

    if(typeof itemsIds === 'object' && open === null) {
      itemsIds.forEach((item) => {
        const selectItem = document.querySelector(`#${elementId} #${item[0]}`)
        if(selectItem) {
          activateElement(selectItem, item[1])
        } else {
          console.log('There is no item with this id in the document.');
          return null;
        }
      })
    }
  }

  function activateElement(item, open, treeNav = false) {
    if(open === true) {
      item.setAttribute('aria-expanded', 'true');
      if(treeNav) {
        const getLink = item.querySelector('a')
        getLink.setAttribute("aria-current", "page");
        getLink.innerHTML = `<strong class="font-bold">${getLink.textContent}</strong>`;
      }
      recursiveParent(item)
    } else {
      item.setAttribute('aria-expanded', 'false');
    }
  }

  function recursiveParent(item) {
    if(item.parentNode.tagName === "UL" && typeof item.parentNode.id === "string" && item.parentNode.id.length === 0) {
      recursiveParent(item.parentNode)
    } else if(item.parentNode.tagName === "LI" && item.parentNode.getAttribute('aria-expanded') === "true") {
      recursiveParent(item.parentNode)
    } else if(item.parentNode.getAttribute('aria-expanded') === "false") {
      item.parentNode.setAttribute('aria-expanded', 'true');
      if(item.parentNode.parentNode.parentNode.getAttribute('aria-expanded') === "false") {
        recursiveParent(item.parentNode)
      }
    }
  }

  function returnMessage() {
    console.log('There is no item with this id in the document.');
    return null;
  }
}

export function accordion(aria) {
  /*
  *   This content is licensed according to the W3C Software License at
  *   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
  *
  *   Simple accordion pattern example
  */

  'use strict';

  Array.prototype.slice.call(document.querySelectorAll('.c-accordion')).forEach(function (accordion) {

    // Allow for multiple accordion sections to be expanded at the same time
    var allowMultiple = accordion.hasAttribute('data-allow-multiple');
    // Allow for each toggle to both open and close individually
    var allowToggle = (allowMultiple) ? allowMultiple : accordion.hasAttribute('data-allow-toggle');

    // Create the array of toggle elements for the accordion group
    var triggers = Array.prototype.slice.call(accordion.querySelectorAll('.c-accordion__trigger'));
    var panels = Array.prototype.slice.call(accordion.querySelectorAll('.c-accordion__panel'));


    accordion.addEventListener('click', function (event) {
      var target = event.target;

      if(target.classList.contains('c-accordion__toggle-all')) {
        toggleAllAccordion(target)
      }

      if (target.classList.contains('c-accordion__trigger')) {
        // Check if the current toggle is expanded.
        var isExpanded = target.getAttribute('aria-expanded') == 'true';
        var active = accordion.querySelector('[aria-expanded="true"]');

        // without allowMultiple, close the open accordion
        if (!allowMultiple && active && active !== target) {
          // Set the expanded state on the triggering element
          active.setAttribute('aria-expanded', 'false');
          //active.querySelector('span').innerText = 'Mostrar';
          active.querySelector('.c-accordion__show').classList.remove('hidden');
          active.querySelector('.c-accordion__hide').classList.add('hidden');
          // Hide the accordion sections, using aria-controls to specify the desired section
          document.getElementById(active.getAttribute('aria-controls')).setAttribute('hidden', '');

          // When toggling is not allowed, clean up disabled state
          if (!allowToggle) {
            active.removeAttribute('aria-disabled');
          }
        }

        if (!isExpanded) {
          // Set the expanded state on the triggering element
          target.setAttribute('aria-expanded', 'true');
          //target.querySelector('span').innerText = 'Ocultar';
          target.querySelector('.c-accordion__show').classList.toggle('hidden');
          target.querySelector('.c-accordion__hide').classList.toggle('hidden');
          // Hide the accordion sections, using aria-controls to specify the desired section
          document.getElementById(target.getAttribute('aria-controls')).removeAttribute('hidden');

          // If toggling is not allowed, set disabled state on trigger
          if (!allowToggle) {
            target.setAttribute('aria-disabled', 'true');
            target.querySelector('.c-accordion__show').classList.add('hidden');
            target.querySelector('.c-accordion__hide').classList.add('hidden');
          }
        }
        else if (allowToggle && isExpanded) {
          // Set the expanded state on the triggering element
          target.setAttribute('aria-expanded', 'false');
          // target.querySelector('span').innerText = 'Mostrar';
          target.querySelector('.c-accordion__show').classList.toggle('hidden');
          target.querySelector('.c-accordion__hide').classList.toggle('hidden');

          // Hide the accordion sections, using aria-controls to specify the desired section
          document.getElementById(target.getAttribute('aria-controls')).setAttribute('hidden', '');
        }

        event.preventDefault();
      }

      if(accordion.querySelector('.c-accordion__toggle-all') && !target.classList.contains('c-accordion__toggle-all')) {
        checkIfAllPanelAreOpenOrClosed()
      }
    });

    // Bind keyboard behaviors on the main accordion container
    accordion.addEventListener('keydown', function (event) {
      var target = event.target;
      var key = event.which.toString();

      var isExpanded = target.getAttribute('aria-expanded') == 'true';
      var allowToggle = (allowMultiple) ? allowMultiple : accordion.hasAttribute('data-allow-toggle');

      // 33 = Page Up, 34 = Page Down
      var ctrlModifier = (event.ctrlKey && key.match(/33|34/));

      // Is this coming from an accordion header?
      if (target.classList.contains('c-accordion__trigger')) {
        // Up/ Down arrow and Control + Page Up/ Page Down keyboard operations
        // 38 = Up, 40 = Down
        if (key.match(/38|40/) || ctrlModifier) {
          var index = triggers.indexOf(target);
          var direction = (key.match(/34|40/)) ? 1 : -1;
          var length = triggers.length;
          var newIndex = (index + length + direction) % length;

          triggers[newIndex].focus();

          event.preventDefault();
        }
        else if (key.match(/35|36/)) {
          // 35 = End, 36 = Home keyboard operations
          switch (key) {
            // Go to first accordion
            case '36':
              triggers[0].focus();
              break;
              // Go to last accordion
            case '35':
              triggers[triggers.length - 1].focus();
              break;
          }
          event.preventDefault();

        }

      }
    });

    // These are used to style the accordion when one of the buttons has focus
    accordion.querySelectorAll('.c-accordion__trigger').forEach(function (trigger) {

      trigger.addEventListener('focus', function (event) {
        accordion.classList.add('focus');
      });

      trigger.addEventListener('blur', function (event) {
        accordion.classList.remove('focus');
      });

    });

    // Minor setup: will set disabled state, via aria-disabled, to an
    // expanded/ active accordion which is not allowed to be toggled close
    if (!allowToggle) {
      // Get the first expanded/ active accordion
      var expanded = accordion.querySelector('[aria-expanded="true"]');

      // If an expanded/ active accordion is found, disable
      if (expanded) {
        expanded.setAttribute('aria-disabled', 'true');
        expanded.querySelector('.c-accordion__show').classList.add('hidden');
        expanded.querySelector('.c-accordion__hide').classList.add('hidden');
      }
    }

    function toggleAllAccordion(target, show = null) {
      const getAllElementsShow = target.parentElement.parentElement.querySelectorAll('.c-accordion__show')
      const getAllElementsHide = target.parentElement.parentElement.querySelectorAll('.c-accordion__hide')
      const getAllPanels = target.parentElement.parentElement.querySelectorAll('.c-accordion__panel')
      const getAllTriggers = target.parentElement.parentElement.querySelectorAll('.c-accordion__trigger')
      if(target.textContent.includes('Mostrar todo') || show === true) {
        getAllElementsShow.forEach(element => {
          element.classList.add('hidden')
        })
        getAllElementsHide.forEach(element => {
          element.classList.remove('hidden')
        })
        getAllPanels.forEach(element => {
          element.removeAttribute('hidden')
        })
        getAllTriggers.forEach(element => {
          element.setAttribute('aria-expanded', 'true');
        })
        target.textContent = 'Ocultar todo'
      } else {
        getAllElementsShow.forEach(element => {
          element.classList.remove('hidden')
        })
        getAllElementsHide.forEach(element => {
          element.classList.add('hidden')
        })
        getAllPanels.forEach(element => {
          element.setAttribute('hidden', "")
        })
        getAllTriggers.forEach(element => {
          element.setAttribute('aria-expanded', 'false');
        })
        target.textContent = 'Mostrar todo'
      }
    }

    function checkIfAllPanelAreOpenOrClosed() {
      const getAccordionTrigger = accordion.querySelectorAll('.c-accordion__trigger');
      let arrayAccordionTrigger = [...getAccordionTrigger].map(element => element.getAttribute('aria-expanded'));

      if(arrayAccordionTrigger.every(element => element === 'true')) {
        accordion.querySelector('.c-accordion__toggle-all').textContent = 'Ocultar todo';
      }

      if(arrayAccordionTrigger.every(element => element === 'false')) {
        accordion.querySelector('.c-accordion__toggle-all').textContent = 'Mostrar todo';
      }
    }

    window.activateItemAccordion = function (menuId, activeItemId) {
      const menu = document.getElementById(menuId);
      if (menu) {
        const activeItem = document.querySelector(`#${menuId} #${activeItemId}`);
        if (activeItem) {
          activateElement(menuId, activeItemId);
          return [menu, activeItem]
        } else {
          console.log('There is no item with this id in the menu.');
          return null;
        }
      } else {
        console.log('There is no accordion with this id in the document.');
        return null;
      }
    };

    window.activateAllAccordion = function (element, show) {
      const button = document.getElementById(element);
      if (button) {
        toggleAllAccordion(button, show);
        return [button, show]
      } else {
        console.log('There is no accordion with this id in the menu.');
        return null;
      }
    };

    function activateElement(menu, activeItem) {
      const getAccordion = document.querySelector(`#${menu}`);
      const allowMultiple = getAccordion.hasAttribute('data-allow-multiple');
      const allowToggle = (allowMultiple) ? allowMultiple : accordion.hasAttribute('data-allow-toggle');
      const selectAllTriggers = document.querySelectorAll(`#${menu} .c-accordion__trigger`);
      [...selectAllTriggers].forEach((trigger) => {
        const getPanel = trigger.parentElement.parentElement.querySelector('.c-accordion__panel');
        const getShowMessage = trigger.querySelector('.c-accordion__show');
        const getHideMessage = trigger.querySelector('.c-accordion__hide');
        if (isActive(trigger)) {
          trigger.setAttribute('aria-expanded', 'true');
          getPanel.removeAttribute('hidden');
          getShowMessage.classList.add('hidden');
          getHideMessage.classList.remove('hidden');
        } else {
          trigger.setAttribute('aria-expanded', 'false');
          getPanel.setAttribute('hidden', 'true');
          getShowMessage.classList.remove('hidden');
          getHideMessage.classList.add('hidden');
        }
      });

      function isActive(element){
        const { id } = element;
        return typeof activeItem === "object" && allowToggle
          ? activeItem.includes(id)
          : id === activeItem;
      }
    }
  });
}

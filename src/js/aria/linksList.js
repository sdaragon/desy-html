export function LinksList(aria) {

  aria.linksListInit = function (domNode) {
    this.rootEl = domNode;
  };

  window.activateItemLinksList = function (wrapperId, activeItemId) {
    const activeItem = document.querySelector(`#${wrapperId} #${activeItemId}`);
    if (activeItem) {
      activateElement(wrapperId, activeItemId);
      return [activeItem];
    } else {
      console.log('There is no element with this id in the menu.');
      return null;
    }
  };

  function activateElement(wrapperId, elementActive) {
    const getWrapper = document.getElementById(wrapperId);
    getWrapper.querySelectorAll('li').forEach((element) => {
      const getLink = element.querySelector('a');
      if (getLink) {
        if (getLink.id === elementActive) {
          wrapActiveElement(getLink);
        } else {
          deactivateElement(getLink);
        }
      }
    });
  };

  function wrapActiveElement(elementActive) {
    const getText = elementActive.querySelector('div[data-element="c-links-list__text"]');
    getText.innerHTML = `<strong class="font-bold">${elementActive.textContent}</strong>`;
  };

  function deactivateElement(elementDeactivated) {
    const getText = elementDeactivated.querySelector('div[data-element="c-links-list__text"]');
    const replaceStrong = getText.textContent.replace('<strong class="font-bold">', '').replace('<strong/>', '');
    getText.textContent = `${replaceStrong}`;
  };
}

export function tabs(aria) {
  /*
  *   This content is licensed according to the W3C Software License at
  *   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
  */
  aria.tabsInit = function (elem) {
    var tablist = elem.querySelectorAll('[role="tablist"]')[0];
    var tabs;
    var panels;

    generateArrays();

    function generateArrays () {
      tabs = elem.querySelectorAll('[role="tab"]');
      panels = elem.querySelectorAll('[role="tabpanel"]');
    };

    /*
    *  Buscamos si las tabs se han inizializado con
    *  la clase c-tabs__link--is-active. En ese caso
    *  añadimos las clases al button y al span
    *  Si ninguna tab contiene la clase c-tabs__link--is-active
    *  activamos la primera tab.
    */
    function checkIfTabIsActive() {
      const getClassFromTabs = Array.from(tabs)
      if(getClassFromTabs.some(tab => tab.classList.contains('c-tabs__link--is-active'))) {
        tabs.forEach((tab) => {
          if(tab.classList.contains('c-tabs__link--is-active')) {
            activateTab(tab, false)
            addActiveClass(tab)
          }
        })
      } else {
        tabs.forEach((tab, index) => {
          if(index === 0) {
            activateTab(tab, false)
            addActiveClass(tab)
          }
        })
      }
    }

    checkIfTabIsActive()

    // For easy reference
    var keys = {
      end: 35,
      home: 36,
      left: 37,
      up: 38,
      right: 39,
      down: 40,
      delete: 46,
      enter: 13,
      space: 32
    };

    // Add or subtract depending on key pressed
    var direction = {
      37: -1,
      38: -1,
      39: 1,
      40: 1
    };

    // Bind listeners
    for (var i = 0; i < tabs.length; ++i) {
      addListeners(i);
    };

    function addListeners (index) {
      tabs[index].addEventListener('click', clickEventListener);
      tabs[index].addEventListener('keydown', keydownEventListener);
      tabs[index].addEventListener('keyup', keyupEventListener);

      // Build an array with all tabs (<button>s) in it
      tabs[index].index = index;
    };

    // When a tab is clicked, activateTab is fired to activate it
    function clickEventListener (event) {
      if(!event.target.matches('.c-tabs__link--is-active, .c-tabs__link--is-active strong')) {
        activateTab(event.target, false);
      }
    };

    // Handle keydown on tabs
    function keydownEventListener (event) {
      var key = event.keyCode;

      switch (key) {
        case keys.end:
          event.preventDefault();
          // Activate last tab
          focusLastTab();
          break;
        case keys.home:
          event.preventDefault();
          // Activate first tab
          focusFirstTab();
          break;

        // Up and down are in keydown
        // because we need to prevent page scroll >:)
        case keys.up:
        case keys.down:
          determineOrientation(event);
          break;
      };
    };

    // Handle keyup on tabs
    function keyupEventListener (event) {
      var key = event.keyCode;

      switch (key) {
        case keys.left:
        case keys.right:
          determineOrientation(event);
          break;
        case keys.delete:
          determineDeletable(event);
          break;
        case keys.enter:
        case keys.space:
          activateTab(event.target);
          break;
      };
    };

    // When a tablistâ€™s aria-orientation is set to vertical,
    // only up and down arrow should function.
    // In all other cases only left and right arrow function.
    function determineOrientation (event) {
      var key = event.keyCode;
      var vertical = tablist.getAttribute('aria-orientation') == 'vertical';
      var proceed = false;

      if (vertical) {
        if (key === keys.up || key === keys.down) {
          event.preventDefault();
          proceed = true;
        };
      }
      else {
        if (key === keys.left || key === keys.right) {
          proceed = true;
        };
      };

      if (proceed) {
        switchTabOnArrowPress(event);
      };
    };

    // Either focus the next, previous, first, or last tab
    // depending on key pressed
    function switchTabOnArrowPress (event) {
      var pressed = event.keyCode;

      if (direction[pressed]) {
        var target = event.target;
        if (target.index !== undefined) {
          if (tabs[target.index + direction[pressed]]) {
            tabs[target.index + direction[pressed]].focus();
          }
          else if (pressed === keys.left || pressed === keys.up) {
            focusLastTab();
          }
          else if (pressed === keys.right || pressed == keys.down) {
            focusFirstTab();
          };
        };
      };
    };

    // Activates any given tab panel
    function activateTab (tab, setFocus) {
      if(setFocus === undefined){
        setFocus = true
      }
      // Deactivate all other tabs
      deactivateTabs();

      // Remove tabindex attribute
      tab.removeAttribute('tabindex');

      // Set the tab as selected
      tab.setAttribute('aria-selected', 'true');

      //Add active class to current tab
      addActiveClass(tab)

      // Get the value of aria-controls (which is an ID)
      var controls = tab.getAttribute('aria-controls');

      // Remove hidden attribute from tab panel to make it visible
      elem.querySelector('#' + controls).removeAttribute('hidden');

      // Set focus when required
      if (setFocus) {
        tab.focus();
      };
    };

    // Deactivate all tabs and tab panels
    function deactivateTabs () {
      for (var t = 0; t < tabs.length; t++) {
        tabs[t].setAttribute('tabindex', '-1');
        tabs[t].setAttribute('aria-selected', 'false');
        removeActiveClass(tabs[t])
      };

      for (var p = 0; p < panels.length; p++) {
        panels[p].setAttribute('hidden', 'hidden');
      };
    };

    // Make a guess
    function focusFirstTab () {
      tabs[0].focus();
    };

    // Make a guess
    function focusLastTab () {
      tabs[tabs.length - 1].focus();
    };

    // Detect if a tab is deletable
    function determineDeletable (event) {
      target = event.target;

      if (target.getAttribute('data-deletable') !== null) {
        // Delete target tab
        deleteTab(event);

        // Update arrays related to tabs widget
        generateArrays();

        // Activate the closest tab to the one that was just deleted
        if (target.index - 1 < 0) {
          activateTab(tabs[0]);
        }
        else {
          activateTab(tabs[target.index - 1]);
        };
      };
    };

    // Deletes a tab and its panel
    function deleteTab (event) {
      var target = event.target;
      var panel = elem.getElementById(target.getAttribute('aria-controls'));

      target.parentElement.removeChild(target);
      panel.parentElement.removeChild(panel);
    };

    // Determine whether there should be a delay
    // when user navigates with the arrow keys
    function determineDelay () {
      var hasDelay = tablist.hasAttribute('data-delay');
      var delay = 0;

      if (hasDelay) {
        var delayValue = tablist.getAttribute('data-delay');
        if (delayValue) {
          delay = delayValue;
        }
        else {
          // If no value is specified, default to 300ms
          delay = 300;
        };
      };

      return delay;
    };

    function addActiveClass(element) {
      //Add active class to current tab
      element.classList.add("c-tabs__link--is-active");
      element.setAttribute('role', 'tab');
      if(element.querySelector('strong') === null) {
        element.innerHTML = `<strong class="font-bold">${element.innerHTML}</strong>` ;
      }
    }

    function removeActiveClass(element) {
      element.classList.remove("c-tabs__link--is-active");
      element.removeAttribute('role');
      if(element.querySelector('strong')) {
        const replaceStrong = element.innerHTML.replace('<strong class="font-bold">', '').replace('<strong/>', '');
        element.innerHTML = `${replaceStrong}`;
      }
    }

    window.activateItemTabs = function (menuId, tabItemId) {
      const menu = document.getElementById(menuId);
      if (menu) {
        const activeItemTab = document.querySelector(`#${menuId} #${tabItemId}`);
        const activeItemPanel = document.querySelector(`[aria-labelledby="${tabItemId}"]`);
        if (activeItemTab) {
          document.querySelectorAll(`#${menuId} .c-tabs__link`).forEach((tab) => {
            removeActiveClass(tab)
          })

          addActiveClass(activeItemTab)

          document.querySelectorAll(`#${menuId} .c-tabs__panel`).forEach((panel) => {
            panel.setAttribute('hidden', 'hidden')
          })

          activeItemPanel.removeAttribute('hidden');

          return [menu, activeItemTab, activeItemPanel];

        } else {
          console.log('There is no element with this id in the menu.');
          return null;
        }
      } else {
        console.log('There is no tabs with this id in the document.');
        return null;
      }
    };
  }

}

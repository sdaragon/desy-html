export function Nav(aria) {
  (function () {
    aria.Nav = function (domNode) {
      this.domNode = domNode;
    };

    aria.Nav.prototype.init = function () {
      this.valueActive = this.domNode.dataset.menuActive;
      if (this.valueActive) {
        this.activateElement(this.valueActive);
      }
    };

    aria.Nav.prototype.activateElement = function (elementActive) {
      this.domNode.querySelectorAll('li').forEach((element) => {
        const getLink = element.querySelector('a') || element.querySelector('span');
        if (getLink) {
          if (getLink.id === elementActive) {
            this.wrapActiveElement(getLink);
          } else {
            this.deactivateElement(getLink);
          }
        }
      });
    };

    aria.Nav.prototype.wrapActiveElement = function (elementActive) {
      if (!elementActive.getAttribute('aria-current')) {
        elementActive.setAttribute('aria-current', 'page');
        elementActive.innerHTML = `<strong class="font-bold">${elementActive.innerHTML}</strong>`;
      }
    };

    aria.Nav.prototype.deactivateElement = function (elementDeactivated) {
      elementDeactivated.removeAttribute('aria-current');
      if(elementDeactivated.querySelector('strong')) {
        const replaceStrong = elementDeactivated.innerHTML.replace('<strong class="font-bold">', '').replace('<strong/>', '');
        elementDeactivated.innerHTML = `${replaceStrong}`;
      }
    };

    window.activateItemNav = function (menuId, activeItemId) {
      const menu = document.getElementById(menuId);
      if (menu) {
        const activeItem = document.querySelector(`#${menuId} #${activeItemId}`);
        if (activeItem) {
          const menuInstance = new aria.Nav(menu);
          menuInstance.activateElement(activeItemId);
          return [menu, activeItem];
        } else {
          console.log('There is no element with this id in the menu.');
          return null;
        }
      } else {
        console.log('There is no nav with this id in the document.');
        return null;
      }
    };
  }());
}

export function HeaderNavigation(aria) {
  (function () {
    aria.HeaderNavigation = function (domNode) {
      this.domNode = domNode;
    };

    aria.HeaderNavigation.prototype.init = function () {
      this.valueActive = this.domNode.dataset.menuActive;
      if (this.valueActive) {
        this.activateElement(this.valueActive);
      }
    };

    aria.HeaderNavigation.prototype.activateElement = function (elementActive) {
      this.domNode.querySelectorAll('li').forEach((element) => {
        const getLink = element.querySelector('a') || element.querySelector('span');
        if (getLink) {
          if (getLink.id === elementActive) {
            this.wrapActiveElement(getLink);
          } else {
            this.deactivateElement(getLink);
          }
        }
      });
    };

    aria.HeaderNavigation.prototype.wrapActiveElement = function (elementActive) {
      elementActive.setAttribute('aria-current', 'page');
      elementActive.innerHTML = `<strong class="font-bold">${elementActive.textContent}</strong>`;
    };

    aria.HeaderNavigation.prototype.deactivateElement = function (elementDeactivated) {
      elementDeactivated.removeAttribute('aria-current');
      elementDeactivated.innerHTML = elementDeactivated.textContent;
    };

    window.activateItemHeaderNavigation = function (menuId, activeItemId) {
      const menu = document.getElementById(menuId);
      if (menu) {
        const activeItem = document.querySelector(`#${menuId} #${activeItemId}`);
        if (activeItem) {
          const menuInstance = new aria.HeaderNavigation(menu);
          menuInstance.activateElement(activeItemId);
          return [menu, activeItem];
        } else {
          console.log('There is no element with this id in the menu.');
          return null;
        }
      } else {
        console.log('There is no menu with this id in the document.');
        return null;
      }
    };
  }());
}

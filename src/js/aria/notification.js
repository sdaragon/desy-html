export function Notification(aria) {

  aria.notificationInit = function (domNode) {
    this.rootEl = domNode;
    /*No todas las notificaciones tienen botón de cerrar
    así que comprobamos que el componente contiene el botón.*/
    if(this.rootEl.querySelector('.c-notification-button__close')) {
      window.addEventListener('click', this.closeNotification.bind(this));
    }
  };

  aria.closeNotification = function (event) {
    const { target } = event;
    const elementNotification = target ? target : event
    if(target) {
      aria.closeNotificationEvent(elementNotification)
    }

    if(!target) {
      aria.closeNotificationWindow(elementNotification)
    }
  }

  aria.closeNotificationEvent = function (element) {
    if(element.classList.contains('c-notification-button__close')) {
      //Replicamos la transición de alpine con tailwind al cerrar.
      element.parentElement.parentElement.parentElement.classList.add('transition-opacity', 'duration-150', 'ease-in-out', 'opacity-0');
      //Añadimos display: none para que el componente "desaparezca del DOM"
      setTimeout(() =>{
        element.parentElement.parentElement.parentElement.classList.add('hidden');
      }, 300)
    }
  }

  aria.closeNotificationWindow = function (element) {
    if(element.querySelector('.c-notification-button__close')) {
      //Replicamos la transición de alpine con tailwind al cerrar.
      element.classList.add('transition-opacity', 'duration-150', 'ease-in-out', 'opacity-0');
      //Añadimos display: none para que el componente "desaparezca del DOM"
      setTimeout(() =>{
        element.classList.add('hidden');
      }, 300)
    }
  }

  window.closeItemNotification = function (element) {
    const notification = document.getElementById(element);
    if (notification) {
      aria.closeNotification(notification)
      return [notification];
    } else {
      console.log('There is no notification with this id in the document.');
      return null;
    }
  };
}

export function Collapsible(aria) {
  aria.collapsibleInit = function (domNode) {
    this.rootEl = domNode;
    this.buttonEl = this.rootEl.querySelector('button[aria-expanded]');
    this.buttonEl.addEventListener('click', handleToggle.bind(this));
    if(this.rootEl.dataset.expanded){
      this.buttonEl.click()
    }
  };

  function handleToggle(element, isOpen = null) {
    const elementTarget = element.type === 'click' ? element.target : element;
    const elementParent = elementTarget.parentNode;
    const button = element.type === 'click' ? element.target : element;
    const elementCollapsibleShow = elementTarget.querySelector('.c-collapsible__show')
    const elementCollapsibleHide = elementTarget.querySelector('.c-collapsible__hide')
    const elementCollapsibleContent = elementTarget.nextElementSibling

    if(element.type === 'click' && elementParent.getAttribute('data-expanded') === 'false' || isOpen) {
      elementParent.setAttribute('data-expanded', 'true')
      button.setAttribute('aria-expanded', 'true')
      elementCollapsibleShow.setAttribute('aria-hidden', 'false')
      elementCollapsibleHide.setAttribute('aria-hidden', 'true')
      elementCollapsibleContent.setAttribute('aria-hidden', 'false')
    } else if (element.type === 'click' && !elementParent.getAttribute('data-expanded') === 'true' || !isOpen) {
      elementParent.setAttribute('data-expanded', 'false')
      button.setAttribute('aria-expanded', 'false')
      elementCollapsibleShow.setAttribute('aria-hidden', 'true')
      elementCollapsibleHide.setAttribute('aria-hidden', 'false')
      elementCollapsibleContent.setAttribute('aria-hidden', 'true')
    }
  }

  window.activateItemCollapsible = function (element, isOpen) {
    const collapsible = document.getElementById(element);
    if (collapsible) {
      handleToggle(collapsible, isOpen)
      return [collapsible, isOpen];
    } else {
      console.log('There is no element with this id in the document.');
      return null;
    }
  };
}

/**
 * Nunjucks globals
 */
import getHTMLCodeFromExample from './get-html-code-from-example.js';
import getNunjucksCodeFromExample from './get-nunjucks-code-from-example.js';
import getHTMLCodeFromFile from './get-html-code-from-file.js';
import getNunjucksCodeFromFile from './get-nunjucks-code-from-file.js';

export const SOURCE_NUNJUCKS_GLOBALS = {
  getNunjucksCodeFromExample,
  getHTMLCodeFromExample,
  getNunjucksCodeFromFile,
  getHTMLCodeFromFile
};

import beautify from 'js-beautify';

/**
 * Component HTML code (formatted)
 *
 * @param {string} templatePath - Path to nunjucks file. Eg: `includes/_loremipsum-large.njk`;
 */
function getHTMLCodeFromFile(templatePath) {
  // Render to HTML
  const html = this.env.render(templatePath).trim();

  // Default beautify options
  const options = beautify.html.defaultOptions();

  return beautify.html(html, {
    indent_size: 2,
    // Ensure nested labels in headings are indented properly
    inline: options.inline.filter((tag) => !['label'].includes(tag)),
    // Remove blank lines
    max_preserve_newlines: 0,
    // Ensure attribute wrapping in header SVG is preserved
    wrap_attributes: 'preserve'
  });
}

export default getHTMLCodeFromFile;

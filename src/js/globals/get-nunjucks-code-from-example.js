import { outdent } from 'outdent';

/**
 * Component Nunjucks code (formatted)
 *
 * @param {string} componentName - Component name
 * @param {unknown} params - Component macro params
 * @returns {string} Nunjucks code
 */
function getNunjucksCodeFromExample(componentName, params, pathName) {
  const macroName = kebabCaseToPascalCase(componentName);

  return outdent`
    {% from "${pathName}s/${componentName}/_macro.${componentName}.njk" import ${pathName}${macroName} %}

    {{ ${pathName}${macroName}(${
    JSON.stringify(params, undefined, 2)
  }) }}
  `;
}

function kebabCaseToPascalCase(value) {
  return value
    .toLowerCase()
    .split('-')
    // capitalize each 'word'
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join('');
}

export default getNunjucksCodeFromExample;

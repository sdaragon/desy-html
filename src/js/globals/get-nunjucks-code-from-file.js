import { outdent } from 'outdent';
import fs from 'fs';

/**
 * Component Nunjucks code (formatted)
 * Uses node.js to read the content of a file, display it as code.
 * @param {string} templatePath - Path to nunjucks file. Eg: `includes/_loremipsum-large.njk`;
 * @returns {string} Nunjucks code
 */
function getNunjucksCodeFromFile(templatePath) {
  let codeFromFile;

  try {
    codeFromFile = fs.readFileSync(`src/templates/${templatePath}`, 'utf8');
  } catch (err) {
    console.error(err);
  }

  return outdent`
    ${codeFromFile}
  `;
}

export default getNunjucksCodeFromFile;

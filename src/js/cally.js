class te {
  /**
   * @type {T}
   */
  #t;
  #e = /* @__PURE__ */ new Set();
  /**
   * @param {T} current
   */
  constructor(t) {
    this.#t = t;
  }
  /**
   * @return {T}
   */
  get current() {
    return this.#t;
  }
  /**
   * @param {T} value
   */
  set current(t) {
    this.#t != t && (this.#t = t, this.#e.forEach((n) => n(t)));
  }
  /**
   * @type {import("hooks").Ref["on"]}
   */
  on(t) {
    return this.#e.add(t), () => this.#e.delete(t);
  }
}
const At = (e) => new te(e), V = Symbol.for("atomico.hooks");
globalThis[V] = globalThis[V] || {};
let k = globalThis[V];
const ee = Symbol.for("Atomico.suspense"), Ft = Symbol.for("Atomico.effect"), ne = Symbol.for("Atomico.layoutEffect"), Rt = Symbol.for("Atomico.insertionEffect"), M = (e, t, n) => {
  const { i: s, hooks: o } = k.c, r = o[s] = o[s] || {};
  return r.value = e(r.value), r.effect = t, r.tag = n, k.c.i++, o[s].value;
}, se = (e) => M((t = At(e)) => t), L = () => M((e = At(k.c.host)) => e), It = () => k.c.update, oe = (e, t, n = 0) => {
  let s = {}, o = !1;
  const r = () => o, c = (i, f) => {
    for (const d in s) {
      const l = s[d];
      l.effect && l.tag === i && (l.value = l.effect(l.value, f));
    }
  };
  return { load: (i) => {
    k.c = { host: t, hooks: s, update: e, i: 0, id: n };
    let f;
    try {
      o = !1, f = i();
    } catch (d) {
      if (d !== ee)
        throw d;
      o = !0;
    } finally {
      k.c = null;
    }
    return f;
  }, cleanEffects: (i) => (c(Rt, i), () => (c(ne, i), () => {
    c(Ft, i);
  })), isSuspense: r };
}, A = Symbol.for;
function Ut(e, t) {
  const n = e.length;
  if (n !== t.length)
    return !1;
  for (let s = 0; s < n; s++) {
    let o = e[s], r = t[s];
    if (o !== r)
      return !1;
  }
  return !0;
}
const C = (e) => typeof e == "function", R = (e) => typeof e == "object", { isArray: re } = Array, tt = (e, t) => (t ? e instanceof HTMLStyleElement : !0) && "hydrate" in (e?.dataset || {});
function $t(e, t) {
  let n;
  const s = (o) => {
    let { length: r } = o;
    for (let c = 0; c < r; c++) {
      const u = o[c];
      if (u && Array.isArray(u))
        s(u);
      else {
        const a = typeof u;
        if (u == null || a === "function" || a === "boolean")
          continue;
        a === "string" || a === "number" ? (n == null && (n = ""), n += u) : (n != null && (t(n), n = null), t(u));
      }
    }
  };
  s(e), n != null && t(n);
}
const Lt = (e, t, n) => (e.addEventListener(t, n), () => e.removeEventListener(t, n));
class _t {
  /**
   *
   * @param {HTMLElement} target
   * @param {string} message
   * @param {string} value
   */
  constructor(t, n, s) {
    this.message = n, this.target = t, this.value = s;
  }
}
class jt extends _t {
}
class ce extends _t {
}
const q = "Custom", ae = null, ie = { true: 1, "": 1, 1: 1 };
function le(e, t, n, s, o) {
  const {
    type: r,
    reflect: c,
    event: u,
    value: a,
    attr: i = ue(t)
  } = n?.name != q && R(n) && n != ae ? n : { type: n }, f = r?.name === q && r.map, d = a != null ? r == Function || !C(a) ? () => a : a : null;
  Object.defineProperty(e, t, {
    configurable: !0,
    /**
     * @this {import("dom").AtomicoThisInternal}
     * @param {any} newValue
     */
    set(l) {
      const m = this[t];
      d && r != Boolean && l == null && (l = d());
      const { error: p, value: y } = (f ? he : me)(
        r,
        l
      );
      if (p && y != null)
        throw new jt(
          this,
          `The value defined for prop '${t}' must be of type '${r.name}'`,
          y
        );
      m != y && (this._props[t] = y ?? void 0, this.update(), u && Yt(this, u), this.updated.then(() => {
        c && (this._ignoreAttr = i, fe(this, r, i, this[t]), this._ignoreAttr = null);
      }));
    },
    /**
     * @this {import("dom").AtomicoThisInternal}
     */
    get() {
      return this._props[t];
    }
  }), d && (o[t] = d()), s[i] = { prop: t, type: r };
}
const Yt = (e, { type: t, base: n = CustomEvent, ...s }) => e.dispatchEvent(new n(t, s)), ue = (e) => e.replace(/([A-Z])/g, "-$1").toLowerCase(), fe = (e, t, n, s) => s == null || t == Boolean && !s ? e.removeAttribute(n) : e.setAttribute(
  n,
  t?.name === q && t?.serialize ? t?.serialize(s) : R(s) ? JSON.stringify(s) : t == Boolean ? "" : s
), de = (e, t) => e == Boolean ? !!ie[t] : e == Number ? Number(t) : e == String ? t : e == Array || e == Object ? JSON.parse(t) : e.name == q ? t : (
  // TODO: If when defining reflect the prop can also be of type string?
  new e(t)
), he = ({ map: e }, t) => {
  try {
    return { value: e(t), error: !1 };
  } catch {
    return { value: t, error: !0 };
  }
}, me = (e, t) => e == null || t == null ? { value: t, error: !1 } : e != String && t === "" ? { value: void 0, error: !1 } : e == Object || e == Array || e == Symbol ? {
  value: t,
  error: {}.toString.call(t) !== `[object ${e.name}]`
} : t instanceof e ? {
  value: t,
  error: e == Number && Number.isNaN(t.valueOf())
} : e == String || e == Number || e == Boolean ? {
  value: t,
  error: e == Number ? typeof t != "number" ? !0 : Number.isNaN(t) : e == String ? typeof t != "string" : typeof t != "boolean"
} : { value: t, error: !0 };
let ye = 0;
const pe = (e) => {
  const t = (e?.dataset || {})?.hydrate || "";
  return t || "c" + ye++;
}, _ = (e, t = HTMLElement) => {
  const n = {}, s = {}, o = "prototype" in t && t.prototype instanceof Element, r = o ? t : "base" in t ? t.base : HTMLElement, { props: c, styles: u } = o ? e : t;
  class a extends r {
    constructor() {
      super(), this._setup(), this._render = () => e({ ...this._props });
      for (const f in s)
        this[f] = s[f];
    }
    /**
     * @returns {import("core").Sheets[]}
     */
    static get styles() {
      return [super.styles, u];
    }
    async _setup() {
      if (this._props)
        return;
      this._props = {};
      let f, d;
      this.mounted = new Promise(
        (E) => this.mount = () => {
          E(), f != this.parentNode && (d != f ? this.unmounted.then(this.update) : this.update()), f = this.parentNode;
        }
      ), this.unmounted = new Promise(
        (E) => this.unmount = () => {
          E(), (f != this.parentNode || !this.isConnected) && (l.cleanEffects(!0)()(), d = this.parentNode, f = null);
        }
      ), this.symbolId = this.symbolId || Symbol(), this.symbolIdParent = Symbol();
      const l = oe(
        () => this.update(),
        this,
        pe(this)
      );
      let m, p = !0;
      const y = tt(this);
      this.update = () => (m || (m = !0, this.updated = (this.updated || this.mounted).then(() => {
        try {
          const E = l.load(this._render), h = l.cleanEffects();
          return E && //@ts-ignore
          E.render(this, this.symbolId, y), m = !1, p && !l.isSuspense() && (p = !1, !y && be(this)), h();
        } finally {
          m = !1;
        }
      }).then(
        /**
         * @param {import("internal/hooks.js").CleanUseEffects} [cleanUseEffect]
         */
        (E) => {
          E && E();
        }
      )), this.updated), this.update();
    }
    connectedCallback() {
      this.mount(), super.connectedCallback && super.connectedCallback();
    }
    disconnectedCallback() {
      super.disconnectedCallback && super.disconnectedCallback(), this.unmount();
    }
    /**
     * @this {import("dom").AtomicoThisInternal}
     * @param {string} attr
     * @param {(string|null)} oldValue
     * @param {(string|null)} value
     */
    attributeChangedCallback(f, d, l) {
      if (n[f]) {
        if (f === this._ignoreAttr || d === l)
          return;
        const { prop: m, type: p } = n[f];
        try {
          this[m] = de(p, l);
        } catch {
          throw new ce(
            this,
            `The value defined as attr '${f}' cannot be parsed by type '${p.name}'`,
            l
          );
        }
      } else
        super.attributeChangedCallback(f, d, l);
    }
    static get props() {
      return { ...super.props, ...c };
    }
    static get observedAttributes() {
      const f = super.observedAttributes || [];
      for (const d in c)
        le(this.prototype, d, c[d], n, s);
      return Object.keys(n).concat(f);
    }
  }
  return a;
};
function be(e) {
  const { styles: t } = e.constructor, { shadowRoot: n } = e;
  if (n && t.length) {
    const s = [];
    $t(t, (o) => {
      o && (o instanceof Element ? n.appendChild(o.cloneNode(!0)) : s.push(o));
    }), s.length && (n.adoptedStyleSheets = s);
  }
}
const qt = (e) => (t, n) => {
  M(
    /**
     * Clean the effect hook
     * @type {import("internal/hooks.js").CollectorEffect}
     */
    ([s, o] = []) => ((o || !o) && (o && Ut(o, n) ? s = s || !0 : (C(s) && s(), s = null)), [s, n]),
    /**
     * @returns {any}
     */
    ([s, o], r) => r ? (C(s) && s(), []) : [s || t(), o],
    e
  );
}, ot = qt(Ft), ge = qt(Rt);
class xt extends Array {
  /**
   *
   * @param {any} initialState
   * @param {(nextState: any, state:any[], mount: boolean )=>void} mapState
   */
  constructor(t, n) {
    let s = !0;
    const o = (r) => {
      try {
        n(r, this, s);
      } finally {
        s = !1;
      }
    };
    super(void 0, o, n), o(t);
  }
  /**
   * The following code allows a mutable approach to useState
   * and useProp this with the idea of allowing an alternative
   * approach similar to Vue or Qwik of state management
   * @todo pending review with the community
   */
  // get value() {
  //     return this[0];
  // }
  // set value(nextState) {
  //     this[2](nextState, this);
  // }
}
const rt = (e) => {
  const t = It();
  return M(
    (n = new xt(e, (s, o, r) => {
      s = C(s) ? s(o[0]) : s, s !== o[0] && (o[0] = s, r || t());
    })) => n
  );
}, P = (e, t) => {
  const [n] = M(([s, o, r = 0] = []) => ((!o || o && !Ut(o, t)) && (s = e()), [s, t, r]));
  return n;
}, ct = (e) => {
  const { current: t } = L();
  if (!(e in t))
    throw new jt(
      t,
      `For useProp("${e}"), the prop does not exist on the host.`,
      e
    );
  return M(
    (n = new xt(t[e], (s, o) => {
      s = C(s) ? s(t[e]) : s, t[e] = s;
    })) => (n[0] = t[e], n)
  );
}, T = (e, t = {}) => {
  const n = L();
  return n[e] || (n[e] = (s = t.detail) => Yt(n.current, {
    type: e,
    ...t,
    detail: s
  })), n[e];
}, et = A("atomico/options");
globalThis[et] = globalThis[et] || {
  sheet: !!document.adoptedStyleSheets
};
const Bt = globalThis[et], Ee = {
  checked: 1,
  value: 1,
  selected: 1
}, De = {
  list: 1,
  type: 1,
  size: 1,
  form: 1,
  width: 1,
  height: 1,
  src: 1,
  href: 1,
  slot: 1
}, Se = {
  shadowDom: 1,
  staticNode: 1,
  cloneNode: 1,
  children: 1,
  key: 1
}, Y = {}, nt = [];
class st extends Text {
}
const ve = A("atomico/id"), I = A("atomico/type"), Z = A("atomico/ref"), zt = A("atomico/vnode"), we = () => {
};
function Ce(e, t, n) {
  return Kt(this, e, t, n);
}
const Ht = (e, t, ...n) => {
  const s = t || Y;
  let { children: o } = s;
  if (o = o ?? (n.length ? n : nt), e === we)
    return o;
  const r = e ? e instanceof Node ? 1 : (
    //@ts-ignore
    e.prototype instanceof HTMLElement && 2
  ) : 0;
  if (r === !1 && e instanceof Function)
    return e(
      o != nt ? { children: o, ...s } : s
    );
  const c = Bt.render || Ce;
  return {
    [I]: zt,
    type: e,
    props: s,
    children: o,
    key: s.key,
    // key for lists by keys
    // define if the node declares its shadowDom
    shadow: s.shadowDom,
    // allows renderings to run only once
    static: s.staticNode,
    // defines whether the type is a childNode `1` or a constructor `2`
    raw: r,
    // defines whether to use the second parameter for document.createElement
    is: s.is,
    // clone the node if it comes from a reference
    clone: s.cloneNode,
    render: c
  };
};
function Kt(e, t, n = ve, s, o) {
  let r;
  if (t && t[n] && t[n].vnode == e || e[I] != zt)
    return t;
  (e || !t) && (o = o || e.type == "svg", r = e.type != "host" && (e.raw == 1 ? (t && e.clone ? t[Z] : t) != e.type : e.raw == 2 ? !(t instanceof e.type) : t ? t[Z] || t.localName != e.type : !t), r && e.type != null && (e.raw == 1 && e.clone ? (s = !0, t = e.type.cloneNode(!0), t[Z] = e.type) : t = e.raw == 1 ? e.type : e.raw == 2 ? new e.type() : o ? document.createElementNS(
    "http://www.w3.org/2000/svg",
    e.type
  ) : document.createElement(
    e.type,
    e.is ? { is: e.is } : void 0
  )));
  const c = t[n] ? t[n] : Y, { vnode: u = Y, cycle: a = 0 } = c;
  let { fragment: i, handlers: f } = c;
  const { children: d = nt, props: l = Y } = u;
  if (f = r ? {} : f || {}, e.static && !r)
    return t;
  if (e.shadow && !t.shadowRoot && // @ts-ignore
  t.attachShadow({ mode: "open", ...e.shadow }), e.props != l && Ne(t, l, e.props, f, o), e.children !== d) {
    const m = e.shadow ? t.shadowRoot : t;
    i = Pe(
      e.children,
      /**
       * @todo for hydration use attribute and send childNodes
       */
      i,
      m,
      n,
      // add support to foreignObject, children will escape from svg
      !a && s,
      o && e.type == "foreignObject" ? !1 : o
    );
  }
  return t[n] = { vnode: e, handlers: f, fragment: i, cycle: a + 1 }, t;
}
function Te(e, t) {
  const n = new st(""), s = new st("");
  let o;
  if (e[t ? "prepend" : "append"](n), t) {
    let { lastElementChild: r } = e;
    for (; r; ) {
      const { previousElementSibling: c } = r;
      if (tt(r, !0) && !tt(c, !0)) {
        o = r;
        break;
      }
      r = c;
    }
  }
  return o ? o.before(s) : e.append(s), {
    markStart: n,
    markEnd: s
  };
}
function Pe(e, t, n, s, o, r) {
  e = e == null ? null : re(e) ? e : [e];
  const c = t || Te(n, o), { markStart: u, markEnd: a, keyes: i } = c;
  let f;
  const d = i && /* @__PURE__ */ new Set();
  let l = u;
  if (e && $t(e, (m) => {
    if (typeof m == "object" && !m[I])
      return;
    const p = m[I] && m.key, y = i && p != null && i.get(p);
    l != a && l === y ? d.delete(l) : l = l == a ? a : l.nextSibling;
    const E = i ? y : l;
    let h = E;
    if (m[I])
      h = Kt(m, E, s, o, r);
    else {
      const w = m + "";
      !(h instanceof Text) || h instanceof st ? h = new Text(w) : h.data != w && (h.data = w);
    }
    h != l && (i && d.delete(h), !E || i ? (n.insertBefore(h, l), i && l != a && d.add(l)) : E == a ? n.insertBefore(h, a) : (n.replaceChild(h, E), l = h)), p != null && (f = f || /* @__PURE__ */ new Map(), f.set(p, h));
  }), l = l == a ? a : l.nextSibling, t && l != a)
    for (; l != a; ) {
      const m = l;
      l = l.nextSibling, m.remove();
    }
  return d && d.forEach((m) => m.remove()), c.keyes = f, c;
}
function Ne(e, t, n, s, o) {
  for (const r in t)
    !(r in n) && St(e, r, t[r], null, o, s);
  for (const r in n)
    St(e, r, t[r], n[r], o, s);
}
function St(e, t, n, s, o, r) {
  if (t = t == "class" && !o ? "className" : t, n = n ?? null, s = s ?? null, t in e && Ee[t] && (n = e[t]), !(s === n || Se[t] || t[0] == "_"))
    if (t[0] == "o" && t[1] == "n" && (C(s) || C(n)))
      ke(e, t.slice(2), s, r);
    else if (t == "ref")
      s && (C(s) ? s(e) : s.current = e);
    else if (t == "style") {
      const { style: c } = e;
      n = n || "", s = s || "";
      const u = R(n), a = R(s);
      if (u)
        for (const i in n)
          if (a)
            !(i in s) && vt(c, i, null);
          else
            break;
      if (a)
        for (const i in s) {
          const f = s[i];
          u && n[i] === f || vt(c, i, f);
        }
      else
        c.cssText = s;
    } else {
      const c = t[0] == "$" ? t.slice(1) : t;
      c === t && (!o && !De[t] && t in e || C(s) || C(n)) ? e[t] = s ?? "" : s == null ? e.removeAttribute(c) : e.setAttribute(
        c,
        R(s) ? JSON.stringify(s) : s
      );
    }
}
function ke(e, t, n, s) {
  if (s.handleEvent || (s.handleEvent = (o) => s[o.type].call(e, o)), n) {
    if (!s[t]) {
      const o = n.capture || n.once || n.passive ? Object.assign({}, n) : null;
      e.addEventListener(t, s, o);
    }
    s[t] = n;
  } else
    s[t] && (e.removeEventListener(t, s), delete s[t]);
}
function vt(e, t, n) {
  let s = "setProperty";
  n == null && (s = "removeProperty", n = null), ~t.indexOf("-") ? e[s](t, n) : e[t] = n;
}
const wt = {};
function B(e, ...t) {
  const n = (e.raw || e).reduce(
    (s, o, r) => s + o + (t[r] || ""),
    ""
  );
  return wt[n] = wt[n] || Oe(n);
}
function Oe(e) {
  if (Bt.sheet) {
    const t = new CSSStyleSheet();
    return t.replaceSync(e), t;
  } else {
    const t = document.createElement("style");
    return t.textContent = e, t;
  }
}
const Me = Ht("host", { style: "display: contents" }), W = A("atomico/context"), Ae = (e, t) => {
  const n = L();
  ge(
    () => Lt(
      n.current,
      "ConnectContext",
      /**
       * @param {CustomEvent<import("context").DetailConnectContext>} event
       */
      (s) => {
        e === s.detail.id && (s.stopPropagation(), s.detail.connect(t));
      }
    ),
    [e]
  );
}, Fe = (e) => {
  const t = T("ConnectContext", {
    bubbles: !0,
    composed: !0
  }), n = () => {
    let r;
    return t({
      id: e,
      connect(c) {
        r = c;
      }
    }), r;
  }, [s, o] = rt(
    n
  );
  return ot(() => {
    s || (e[W] || (e[W] = customElements.whenDefined(
      new e().localName
    )), e[W].then(
      () => o(n)
    ));
  }, [e]), s;
}, Re = (e) => {
  const t = Fe(e), n = It();
  return ot(() => {
    if (t)
      return Lt(t, "UpdatedValue", n);
  }, [t]), (t || e).value;
}, Ie = (e) => {
  const t = _(
    () => (Ae(t, L().current), Me),
    {
      props: {
        value: {
          type: Object,
          event: { type: "UpdatedValue" },
          value: () => e
        }
      }
    }
  );
  return t.value = e, t;
}, b = (e, t, n) => (t == null ? t = { key: n } : t.key = n, Ht(e, t)), U = b, Jt = B`*,*:before,*:after{box-sizing:border-box}button{padding:0;touch-action:manipulation;cursor:pointer;user-select:none}`, Zt = B`.vh{position:absolute;transform:scale(0)}`;
function at() {
  return D.from(/* @__PURE__ */ new Date());
}
function it(e, t = 0) {
  const n = v(e), s = n.getUTCDay(), o = (s < t ? 7 : 0) + s - t;
  return n.setUTCDate(n.getUTCDate() - o), D.from(n);
}
function Wt(e, t = 0) {
  return it(e, t).add({ days: 6 });
}
function Xt(e) {
  return D.from(new Date(Date.UTC(e.year, e.month, 0)));
}
function z(e, t, n) {
  return t && D.compare(e, t) < 0 ? t : n && D.compare(e, n) > 0 ? n : e;
}
const Ue = { days: 1 };
function $e(e, t = 0) {
  let n = it(e.toPlainDate(), t);
  const s = Wt(Xt(e), t), o = [];
  for (; D.compare(n, s) < 0; ) {
    const r = [];
    for (let c = 0; c < 7; c++)
      r.push(n), n = n.add(Ue);
    o.push(r);
  }
  return o;
}
function v(e) {
  return new Date(Date.UTC(e.year, e.month - 1, e.day ?? 1));
}
const Le = /^(\d{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[0-1])$/, X = (e, t) => e.toString().padStart(t, "0");
class D {
  constructor(t, n, s) {
    this.year = t, this.month = n, this.day = s;
  }
  // this is an incomplete implementation that only handles arithmetic on a single unit at a time.
  // i didn't want to get into more complex arithmetic since it get tricky fast
  // this is enough to serve my needs and will still be a drop-in replacement when actual Temporal API lands
  add(t) {
    const n = v(this);
    if ("days" in t)
      return n.setUTCDate(this.day + t.days), D.from(n);
    let { year: s, month: o } = this;
    "months" in t ? (o = this.month + t.months, n.setUTCMonth(o - 1)) : (s = this.year + t.years, n.setUTCFullYear(s));
    const r = D.from(v({ year: s, month: o, day: 1 }));
    return z(D.from(n), r, Xt(r));
  }
  toString() {
    return `${X(this.year, 4)}-${X(this.month, 2)}-${X(this.day, 2)}`;
  }
  toPlainYearMonth() {
    return new H(this.year, this.month);
  }
  equals(t) {
    return D.compare(this, t) === 0;
  }
  static compare(t, n) {
    return t.year < n.year ? -1 : t.year > n.year ? 1 : t.month < n.month ? -1 : t.month > n.month ? 1 : t.day < n.day ? -1 : t.day > n.day ? 1 : 0;
  }
  static from(t) {
    if (typeof t == "string") {
      const n = t.match(Le);
      if (!n)
        throw new TypeError(t);
      const [, s, o, r] = n;
      return new D(
        parseInt(s, 10),
        parseInt(o, 10),
        parseInt(r, 10)
      );
    }
    return new D(
      t.getUTCFullYear(),
      t.getUTCMonth() + 1,
      t.getUTCDate()
    );
  }
}
class H {
  constructor(t, n) {
    this.year = t, this.month = n;
  }
  add(t) {
    const n = v(this), s = (t.months ?? 0) + (t.years ?? 0) * 12;
    return n.setUTCMonth(n.getUTCMonth() + s), new H(n.getUTCFullYear(), n.getUTCMonth() + 1);
  }
  equals(t) {
    return this.year === t.year && this.month === t.month;
  }
  toPlainDate() {
    return new D(this.year, this.month, 1);
  }
}
function x(e, t) {
  if (t)
    try {
      return e.from(t);
    } catch {
    }
}
function O(e) {
  const [t, n] = ct(e);
  return [P(() => x(D, t), [t]), (r) => n(r.toString())];
}
function _e(e) {
  const [t = "", n] = ct(e);
  return [P(() => {
    const [r, c] = t.split("/"), u = x(D, r), a = x(D, c);
    return u && a ? [u, a] : [];
  }, [t]), (r) => n(`${r[0]}/${r[1]}`)];
}
function je(e) {
  const [t = "", n] = ct(e);
  return [P(() => {
    const r = [];
    for (const c of t.trim().split(/\s+/)) {
      const u = x(D, c);
      u && r.push(u);
    }
    return r;
  }, [t]), (r) => n(r.join(" "))];
}
function $(e, t) {
  return P(
    () => new Intl.DateTimeFormat(t, { timeZone: "UTC", ...e }),
    [t, e]
  );
}
function Ct(e, t, n) {
  const s = $(e, n);
  return P(() => {
    const o = [], r = /* @__PURE__ */ new Date();
    for (var c = 0; c < 7; c++) {
      const u = (r.getUTCDay() - t + 7) % 7;
      o[u] = s.format(r), r.setUTCDate(r.getUTCDate() + 1);
    }
    return o;
  }, [t, s]);
}
const Tt = (e, t, n) => z(e, t, n) === e, Pt = (e) => e.target.matches(":dir(ltr)"), Ye = { month: "long", day: "numeric" }, qe = { month: "long" }, xe = { weekday: "narrow" }, Be = { weekday: "long" }, G = { bubbles: !0 };
function ze({ props: e, context: t }) {
  const { offset: n } = e, {
    firstDayOfWeek: s,
    isDateDisallowed: o,
    min: r,
    max: c,
    page: u,
    locale: a,
    focusedDate: i
  } = t, f = at(), d = Ct(Be, s, a), l = Ct(xe, s, a), m = $(Ye, a), p = $(qe, a), y = P(
    () => u.start.add({ months: n }),
    [u, n]
  ), E = P(
    () => $e(y, s),
    [y, s]
  ), h = T("focusday", G), w = T("selectday", G), K = T("hoverday", G);
  function mt(g) {
    h(z(g, r, c));
  }
  function Gt(g) {
    let S;
    switch (g.key) {
      case "ArrowRight":
        S = i.add({ days: Pt(g) ? 1 : -1 });
        break;
      case "ArrowLeft":
        S = i.add({ days: Pt(g) ? -1 : 1 });
        break;
      case "ArrowDown":
        S = i.add({ days: 7 });
        break;
      case "ArrowUp":
        S = i.add({ days: -7 });
        break;
      case "PageUp":
        S = i.add(g.shiftKey ? { years: -1 } : { months: -1 });
        break;
      case "PageDown":
        S = i.add(g.shiftKey ? { years: 1 } : { months: 1 });
        break;
      case "Home":
        S = it(i, s);
        break;
      case "End":
        S = Wt(i, s);
        break;
      default:
        return;
    }
    mt(S), g.preventDefault();
  }
  function Qt(g) {
    const S = y.equals(g);
    if (!t.showOutsideDays && !S)
      return;
    const Vt = g.equals(i), yt = g.equals(f), pt = v(g), j = o?.(pt), bt = !Tt(g, r, c);
    let gt = "", N;
    if (t.type === "range") {
      const [F, J] = t.value, Et = F?.equals(g), Dt = J?.equals(g);
      N = F && J && Tt(g, F, J), gt = `${Et ? "range-start" : ""} ${Dt ? "range-end" : ""} ${N && !Et && !Dt ? "range-inner" : ""}`;
    } else
      t.type === "multi" ? N = t.value.some((F) => F.equals(g)) : N = t.value?.equals(g);
    return {
      part: `${`button day ${// we don't want outside days to ever be shown as selected
      S ? N ? "selected" : "" : "outside"} ${j ? "disallowed" : ""} ${yt ? "today" : ""}`} ${gt}`,
      tabindex: S && Vt ? 0 : -1,
      disabled: bt,
      "aria-disabled": j ? "true" : void 0,
      "aria-pressed": S && N,
      "aria-current": yt ? "date" : void 0,
      "aria-label": m.format(pt),
      onkeydown: Gt,
      onclick() {
        j || w(g), mt(g);
      },
      onmouseover() {
        !j && !bt && K(g);
      }
    };
  }
  return {
    weeks: E,
    yearMonth: y,
    daysLong: d,
    daysShort: l,
    formatter: p,
    getDayProps: Qt
  };
}
const Q = at(), lt = Ie({
  type: "date",
  firstDayOfWeek: 1,
  isDateDisallowed: () => !1,
  focusedDate: Q,
  page: { start: Q.toPlainYearMonth(), end: Q.toPlainYearMonth() }
});
customElements.define("calendar-month-ctx", lt);
const He = _(
  (e) => {
    const t = Re(lt), n = se(), s = ze({ props: e, context: t });
    function o() {
      n.current.querySelector("button[tabindex='0']")?.focus();
    }
    return /* @__PURE__ */ U("host", { shadowDom: !0, focus: o, children: [
      /* @__PURE__ */ b("div", { id: "h", part: "heading", children: s.formatter.format(v(s.yearMonth)) }),
      /* @__PURE__ */ U("table", { ref: n, "aria-labelledby": "h", part: "table", children: [
        /* @__PURE__ */ b("thead", { children: /* @__PURE__ */ b("tr", { part: "tr head", children: s.daysLong.map((r, c) => /* @__PURE__ */ U("th", { part: "th", scope: "col", children: [
          /* @__PURE__ */ b("span", { class: "vh", children: r }),
          /* @__PURE__ */ b("span", { "aria-hidden": "true", children: s.daysShort[c] })
        ] })) }) }),
        /* @__PURE__ */ b("tbody", { children: s.weeks.map((r, c) => /* @__PURE__ */ b("tr", { part: "tr week", children: r.map((u, a) => {
          const i = s.getDayProps(u);
          return /* @__PURE__ */ b("td", { part: "td", children: i && /* @__PURE__ */ b("button", { ...i, children: u.day }) }, a);
        }) }, c)) })
      ] })
    ] });
  },
  {
    props: {
      offset: {
        type: Number,
        value: 0
      }
    },
    styles: [
      Jt,
      Zt,
      B`:host{--color-accent: black;--color-text-on-accent: white;display:flex;flex-direction:column;gap:.25rem;text-align:center;inline-size:fit-content}table{border-collapse:collapse;font-size:.875rem}th{font-weight:700;block-size:2.25rem}td{padding-inline:0}button{color:inherit;font-size:inherit;background:transparent;border:0;font-variant-numeric:tabular-nums;block-size:2.25rem;inline-size:2.25rem}button:hover:where(:not(:disabled,[aria-disabled])){background:#0000000d}button:is([aria-pressed=true],:focus-visible){background:var(--color-accent);color:var(--color-text-on-accent)}button:focus-visible{outline:1px solid var(--color-text-on-accent);outline-offset:-2px}button:disabled,:host::part(outside),:host::part(disallowed){cursor:default;opacity:.5}`
    ]
  }
);
customElements.define("calendar-month", He);
function Nt(e) {
  return /* @__PURE__ */ b(
    "button",
    {
      part: `button ${e.name} ${e.onclick ? "" : "disabled"}`,
      onclick: e.onclick,
      "aria-disabled": e.onclick ? null : "true",
      children: /* @__PURE__ */ b("slot", { name: e.name, children: e.children })
    }
  );
}
function ut(e) {
  const t = v(e.page.start), n = v(e.page.end);
  return /* @__PURE__ */ U("div", { role: "group", "aria-labelledby": "h", part: "container", children: [
    /* @__PURE__ */ b("div", { id: "h", class: "vh", "aria-live": "polite", "aria-atomic": "true", children: e.formatVerbose.formatRange(t, n) }),
    /* @__PURE__ */ U("div", { part: "header", children: [
      /* @__PURE__ */ b(Nt, { name: "previous", onclick: e.previous, children: "Previous" }),
      /* @__PURE__ */ b("slot", { part: "heading", name: "heading", children: /* @__PURE__ */ b("div", { "aria-hidden": "true", children: e.format.formatRange(t, n) }) }),
      /* @__PURE__ */ b(Nt, { name: "next", onclick: e.next, children: "Next" })
    ] }),
    /* @__PURE__ */ b(
      lt,
      {
        value: e,
        onselectday: e.onSelect,
        onfocusday: e.onFocus,
        onhoverday: e.onHover,
        children: /* @__PURE__ */ b("slot", {})
      }
    )
  ] });
}
const ft = {
  value: {
    type: String,
    value: ""
  },
  min: {
    type: String,
    value: ""
  },
  max: {
    type: String,
    value: ""
  },
  isDateDisallowed: {
    type: Function,
    value: (e) => !1
  },
  firstDayOfWeek: {
    type: Number,
    value: () => 1
  },
  showOutsideDays: {
    type: Boolean,
    value: !1
  },
  locale: {
    type: String,
    value: () => {
    }
  },
  months: {
    type: Number,
    value: 1
  },
  focusedDate: {
    type: String,
    value: () => {
    }
  }
}, dt = [
  Jt,
  Zt,
  B`:host{display:block;inline-size:fit-content}[role=group]{display:flex;flex-direction:column;gap:1em}:host::part(header){display:flex;align-items:center;justify-content:space-between}:host::part(heading){font-weight:700;font-size:1.25em}button{display:flex;align-items:center;justify-content:center}button[aria-disabled]{cursor:default;opacity:.5}`
], Ke = { year: "numeric" }, Je = { year: "numeric", month: "long" };
function kt(e, t) {
  return (t.year - e.year) * 12 + t.month - e.month;
}
const Ot = (e, t) => (e = t === 12 ? new H(e.year, 1) : e, {
  start: e,
  end: e.add({ months: t - 1 })
});
function ht({
  months: e,
  locale: t,
  focusedDate: n,
  setFocusedDate: s
}) {
  const [o] = O("min"), [r] = O("max"), c = T("focusday"), u = T("change"), a = P(
    () => z(n ?? at(), o, r),
    [n, o, r]
  ), [i, f] = rt(
    () => Ot(a.toPlainYearMonth(), e)
  ), d = (h) => {
    const w = kt(i.start, h.toPlainYearMonth());
    return w >= 0 && w < e;
  };
  ot(() => {
    let h = i.start;
    if (!d(a)) {
      const w = kt(h, a.toPlainYearMonth()), K = Math.floor(w / e);
      h = h.add({ months: K * e });
    }
    f(Ot(h, e));
  }, [a, e]);
  const l = L();
  function m() {
    l.current.querySelectorAll("calendar-month").forEach((h) => h.focus());
  }
  function p(h) {
    s(h), c(v(h));
  }
  const y = $(Ke, t), E = $(Je, t);
  return {
    format: y,
    formatVerbose: E,
    page: i,
    focusedDate: a,
    dispatch: u,
    onFocus(h) {
      h.stopPropagation(), p(h.detail), setTimeout(m);
    },
    min: o,
    max: r,
    next: r == null || !d(r) ? () => p(a.add({ months: e })) : void 0,
    previous: o == null || !d(o) ? () => p(a.add({ months: -e })) : void 0,
    focus: m
  };
}
const Ze = _(
  (e) => {
    const [t, n] = O("value"), [s = t, o] = O("focusedDate"), r = ht({
      ...e,
      focusedDate: s,
      setFocusedDate: o
    });
    function c(u) {
      n(u.detail), r.dispatch();
    }
    return /* @__PURE__ */ b("host", { shadowDom: !0, focus: r.focus, children: /* @__PURE__ */ b(
      ut,
      {
        ...e,
        ...r,
        type: "date",
        value: t,
        onSelect: c
      }
    ) });
  },
  { props: ft, styles: dt }
);
customElements.define("calendar-date", Ze);
const Mt = (e, t) => D.compare(e, t) < 0 ? [e, t] : [t, e], We = _(
  (e) => {
    const [t, n] = _e("value"), [s = t[0], o] = O("focusedDate"), r = ht({
      ...e,
      focusedDate: s,
      setFocusedDate: o
    }), c = T("rangestart"), u = T("rangeend"), [a, i] = rt();
    function f(p) {
      r.onFocus(p), d(p);
    }
    function d(p) {
      p.stopPropagation(), i((y) => y && { ...y, b: p.detail });
    }
    function l(p) {
      const y = p.detail;
      p.stopPropagation(), a ? (n(Mt(a.a, y)), i(void 0), u(v(y)), r.dispatch()) : (i({ a: y, b: y }), c(v(y)));
    }
    const m = a ? Mt(a.a, a.b) : t;
    return /* @__PURE__ */ b("host", { shadowDom: !0, focus: r.focus, children: /* @__PURE__ */ b(
      ut,
      {
        ...e,
        ...r,
        type: "range",
        value: m,
        onFocus: f,
        onHover: d,
        onSelect: l
      }
    ) });
  },
  { props: ft, styles: dt }
);
customElements.define("calendar-range", We);
const Xe = _(
  (e) => {
    const [t, n] = je("value"), [s = t[0], o] = O("focusedDate"), r = ht({
      ...e,
      focusedDate: s,
      setFocusedDate: o
    });
    function c(u) {
      const a = [...t], i = t.findIndex((f) => f.equals(u.detail));
      i < 0 ? a.push(u.detail) : a.splice(i, 1), n(a), r.dispatch();
    }
    return /* @__PURE__ */ b("host", { shadowDom: !0, focus: r.focus, children: /* @__PURE__ */ b(
      ut,
      {
        ...e,
        ...r,
        type: "multi",
        value: t,
        onSelect: c
      }
    ) });
  },
  { props: ft, styles: dt }
);
customElements.define("calendar-multi", Xe);
export {
  Ze as CalendarDate,
  He as CalendarMonth,
  Xe as CalendarMulti,
  We as CalendarRange
};
